BUILD_PATH = ../build

# include qscintilla framework
include( ../qscintilla/qscintilla.pri )

TEMPLATE    = app
TARGET      = GUIDE
CONFIG      *= qt warn_on thread console precompile_header
QT          += gui core
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets printsupport
}
CONFIG              -= console
VPATH               = ../miparser
PRECOMPILED_HEADER  = include/stable.h

QMAKE_TARGET_COMPANY        = "GAIT of NLSDE, BUAA"
QMAKE_TARGET_PRODUCT        = "GUIDE"
QMAKE_TARGET_DESCRIPTION    = "GUIDE is an Integrated Development Environment of C, C++, Pascal, especially for NOI/IOI, the National/International Olympics of Informatics, users."
QMAKE_TARGET_COPYRIGHT      = "Copyright (C) 2008 - 2009 Richard R. Zhang"

COPYRIGHTS  = "Copyright (C) 2008 - 2009 Richard R. Zhang,XY ZHOU,P.P. ZHAI, Remi Liu"
#DOMAIN     = "http://www.assembla.com/spaces/manna"
DOMAIN      = "http://gait.buaa.edu.cn/liuzhibin"
VERSION     = 1.1.0
NAME        = "GUIDE"
AUTHOR      = "Richard R. Zhang,XY Y ZHOUYU,P.P. ZHAI, Remi Liu"
DESCRIPTION = "GUIDE is an Integrated Development Environment of C, C++, Pascal, especially for NOI/IOI, the National/International Olympics of Informatics, users."

DEFINES *= "PROGRAM_NAME=\"\\\"$${NAME}\\\"\"" "PROGRAM_VERSION=\"\\\"$${VERSION}\\\"\"" "PROGRAM_DOMAIN=\"\\\"$${DOMAIN}\\\"\"" "PROGRAM_COPYRIGHTS=\"\\\"$${COPYRIGHTS}\\\"\"" "PROGRAM_AUTHOR=\"\\\"$${AUTHOR}\\\"\"" "PROGRAM_DESCRIPTION=\"\\\"$${DESCRIPTION}\\\"\""

LIBS                    *= -L$${BUILD_PATH}
*-g++:LIBS              *= -Wl,--whole-archive # import all symbols as the not used ones too
mac:*-g++:LIBS          *= -dynamic
else:unix:*-g++:LIBS    *= -rdynamic

PRE_TARGETDEPS  *= ../qscintilla

DESTDIR     = ../bin
INCLUDEPATH *= $${PWD}/include ../miparser

*-g++:QMAKE_CXXFLAGS += -pipe
win32-msvc*:QMAKE_CXXFLAGS += /MP

CONFIG(debug, debug|release) {
    #Debug
    CONFIG  += console
    unix:TARGET = $$join(TARGET,,,_debug)
    else:TARGET = $$join(TARGET,,,d)
    unix:OBJECTS_DIR    = $${BUILD_PATH}/debug/.obj/unix
    win32:OBJECTS_DIR   = $${BUILD_PATH}/debug/.obj/win32
    mac:OBJECTS_DIR = $${BUILD_PATH}/debug/.obj/mac
    UI_DIR  = $${BUILD_PATH}/debug/.ui
    MOC_DIR = $${BUILD_PATH}/debug/.moc
    RCC_DIR = $${BUILD_PATH}/debug/.rcc
    unix:LIBS   *= -lqscintilla2_debug
    else:LIBS   *= -lqscintilla2d
    win32-g++:LIBS  *= -Wl,--out-implib,$${BUILD_PATH}/lib$${TARGET}.a
    win32-msvc*:LIBS *= /IMPLIB:$${BUILD_PATH}/$${TARGET}.lib -lshell32
}
CONFIG(release, debug|release) {
    #Release
    unix:OBJECTS_DIR    = $${BUILD_PATH}/release/.obj/unix
    win32:OBJECTS_DIR   = $${BUILD_PATH}/release/.obj/win32
    mac:OBJECTS_DIR = $${BUILD_PATH}/release/.obj/mac
    UI_DIR  = $${BUILD_PATH}/release/.ui
    MOC_DIR = $${BUILD_PATH}/release/.moc
    RCC_DIR = $${BUILD_PATH}/release/.rcc
    LIBS    *= -lqscintilla2
    win32-g++:LIBS  *= -Wl,--out-implib,$${BUILD_PATH}/lib$${TARGET}.a
    win32-msvc*:LIBS    *= /IMPLIB:$${BUILD_PATH}/$${TARGET}.lib -lshell32
}
mac:*-g++:LIBS    *= -Wl,-noall_load # stop importing all symbols
else:*-g++:LIBS    *= -Wl,--no-whole-archive # stop importing all symbols

# include install script
#include( installs.pri )
#DEFINES    *= "PROGRAM_PREFIX=\"\\\"$${PROGRAM_PREFIX}\\\"\"" "PROGRAM_DATAS=\"\\\"$${PROGRAM_DATAS}\\\"\""

FORMS = \
    ui/mainwindow.ui \
    ui/registerswatch.ui \
    ui/findwidget.ui \
    ui/externtool.ui \
    ui/settingdlg.ui \
    ui/fileoptiondlg.ui \
    ui/Set_startnum.ui \
    ui/breakpoints.ui \
    ui/bkpts.ui \
    ui/filebrowser.ui \
    ui/helpwidget.ui

HEADERS = \
    include/sourcetype.h \
    include/mainwindow.h \
    include/editortabwidget.h \
    include/meditor.h \
    include/buildlog.h \
    include/build.h \
    include/treecombobox.h \
    include/filebrowser.h \
    include/findwidget.h \
    include/misc.h \
    include/externtool.h \
    include/executer.h \
    include/helpwidget.h \
    include/mannasettings.h \
    include/settingdlg.h \
    include/fileoption.h \
    include/fileoptiondlg.h \
    include/gdbmi.h \
    include/gdbmanager.h \
    include/varhelper.h \
    include/stable.h \
    include/eventhandler.h \
    include/set_startnum.h \
    midef.h \
    include/breakpointsmanager.h \
    include/bkptsmanager.h

SOURCES = \
    src/main.cpp \
    src/mainwindow.cpp \
    src/editortabwidget.cpp \
    src/meditor.cpp \
    src/buildlog.cpp \
    src/build.cpp \
    src/treecombobox.cpp \
    src/filebrowser.cpp \
    src/findwidget.cpp \
    src/misc.cpp \
    src/externtool.cpp \
    src/executer.cpp \
    src/helpwidget.cpp \
    src/mannasettings.cpp \
    src/settingdlg.cpp \
    src/fileoption.cpp \
    src/fileoptiondlg.cpp \
    src/gdbmi.cpp \
    src/gdbmanager.cpp \
    src/varhelper.cpp \
    src/eventhandler.cpp \
    src/set_startnum.cpp \
    milexer.cpp \
    miparser.cpp \
    mires.cpp \
    src/breakpointsmanager.cpp \
    src/bkptsmanager.cpp

RESOURCES    = resources/Manna.qrc

win32:RC_FILE    *= resources/Manna.rc

unix:ICON    = resources/images/application/Manna.png
win32:ICON    = resources/images/application/Manna.ico

TRANSLATIONS = lang_zh_CN.ts qt_zh_CN.ts
