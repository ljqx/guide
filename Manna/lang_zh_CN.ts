<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>BkptsDialog</name>
    <message>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>All</source>
        <translation>全选</translation>
    </message>
    <message>
        <source>Invert</source>
        <translation>反选</translation>
    </message>
    <message>
        <source>Breakpoints Manager</source>
        <translation>断点管理</translation>
    </message>
</context>
<context>
    <name>BreakpointsManager</name>
    <message>
        <source>Close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
    <message>
        <source>Delete selected breakpoints</source>
        <translation type="obsolete">删除选定断点</translation>
    </message>
    <message>
        <source>Delete All</source>
        <translation type="obsolete">删除全部</translation>
    </message>
    <message>
        <source>Delete selected file&apos;s all breakpoints</source>
        <translation type="obsolete">删除选定文件的所有断点</translation>
    </message>
</context>
<context>
    <name>BreakpointsModel</name>
    <message>
        <source>Line</source>
        <translation>行号</translation>
    </message>
    <message>
        <source>Condition</source>
        <translation>条件</translation>
    </message>
</context>
<context>
    <name>Build</name>
    <message>
        <source>Start Build</source>
        <translation>开始编译</translation>
    </message>
    <message>
        <source>Build stopped</source>
        <translation>编译结束</translation>
    </message>
</context>
<context>
    <name>BuildLog</name>
    <message>
        <source>error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>note</source>
        <translation>提示</translation>
    </message>
</context>
<context>
    <name>EditorTabWidget</name>
    <message>
        <source>Close Tab</source>
        <translation>关闭当前页</translation>
    </message>
    <message>
        <source>Close Other Tabs</source>
        <translation>除此页之外全部关闭</translation>
    </message>
    <message>
        <source>Close All Tabs</source>
        <translation>全部关闭</translation>
    </message>
    <message>
        <source>Set file options</source>
        <translation>文件设置</translation>
    </message>
    <message>
        <source>Untitled</source>
        <translation>未命名</translation>
    </message>
    <message>
        <source>All Files</source>
        <translation>全部文件</translation>
    </message>
    <message>
        <source>Save..</source>
        <translation>保存..</translation>
    </message>
    <message>
        <source>C/C++ Sources</source>
        <translation>C/C++程序</translation>
    </message>
    <message>
        <source>Pascal Sources</source>
        <translation>Pascal程序</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>文本文件</translation>
    </message>
    <message>
        <source>File has been modified outside...</source>
        <translation>当前文件在GUIDE之外被修改...</translation>
    </message>
    <message>
        <source>The file %1 has been modified outside.
Do you want to reload it?</source>
        <translation>%1
该文件在GUIDE之外被修改。
是否要重新加载它？</translation>
    </message>
</context>
<context>
    <name>ExternTool</name>
    <message>
        <source>External Tools</source>
        <translation>外部工具</translation>
    </message>
    <message>
        <source>Please enter the path to the external programs:</source>
        <translation>请输入外部工具的文件路径:</translation>
    </message>
    <message>
        <source>&amp;Test</source>
        <translation>测试(&amp;T)</translation>
    </message>
    <message>
        <source>Don&apos;t show this next time.</source>
        <translation>下次不再提示.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Please select the directory</source>
        <translation>请选择文件路径</translation>
    </message>
    <message>
        <source>Can not find gcc!</source>
        <translation>无法找到gcc!</translation>
    </message>
    <message>
        <source>Can not find g++!</source>
        <translation>无法找到g++!</translation>
    </message>
    <message>
        <source>Can not find gdb!</source>
        <translation>无法找到gdb!</translation>
    </message>
    <message>
        <source>Can not find fpc!</source>
        <translation>无法找到fpc!</translation>
    </message>
</context>
<context>
    <name>FileBrowser</name>
    <message>
        <source>Go Up</source>
        <translation>向上</translation>
    </message>
    <message>
        <source>Refresh current view</source>
        <translation>刷新</translation>
    </message>
    <message>
        <source>Quick Navigation</source>
        <translation>快速定位</translation>
    </message>
    <message>
        <source>Set selected item as root</source>
        <translation>选择根目录</translation>
    </message>
    <message>
        <source>File Browser</source>
        <translation>文件夹</translation>
    </message>
</context>
<context>
    <name>FileOptionDlg</name>
    <message>
        <source>File Options</source>
        <translation>文件设置</translation>
    </message>
    <message>
        <source>Compile Command:</source>
        <translation>编译命令:</translation>
    </message>
    <message>
        <source>Cmd</source>
        <translation>命令</translation>
    </message>
    <message>
        <source>Compile Option:</source>
        <translation>编译选项:</translation>
    </message>
    <message>
        <source>Execution Option:</source>
        <translation>运行参数:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>FilenameListModel</name>
    <message>
        <source>Untitled</source>
        <translation type="obsolete">未命名</translation>
    </message>
    <message>
        <source>File Name</source>
        <translation type="obsolete">文件名</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一个</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>下一个</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <source>ReplaceAll</source>
        <translation>替换全部</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Regular Expression</source>
        <translation>正则表达式</translation>
    </message>
    <message>
        <source>Whole words</source>
        <translation>全词匹配</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation>区分大小写</translation>
    </message>
    <message>
        <source>Find:</source>
        <translation>查找:</translation>
    </message>
    <message>
        <source>replaced by:</source>
        <translation>替换为:</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <source>Back</source>
        <translation>向后</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>向前</translation>
    </message>
    <message>
        <source>Home</source>
        <translation>主页</translation>
    </message>
    <message>
        <source>Refresh current view</source>
        <translation>刷新</translation>
    </message>
    <message>
        <source>Enter</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Help Browser</source>
        <translation>帮助浏览</translation>
    </message>
</context>
<context>
    <name>MEditor</name>
    <message>
        <source>Open file...</source>
        <translation>打开文件...</translation>
    </message>
    <message>
        <source>Cannot read file %1:
%2.</source>
        <translation>无法读取文件%1:
%2.</translation>
    </message>
    <message>
        <source>Save file...</source>
        <translation>保存文件...</translation>
    </message>
    <message>
        <source>Cannot write file %1:
%2.</source>
        <translation>无法写入文件%1:
%2.</translation>
    </message>
    <message>
        <source>Save backup...</source>
        <translation>保存备份...</translation>
    </message>
    <message>
        <source>File has been modified...</source>
        <translation>文件已经被修改...</translation>
    </message>
    <message>
        <source>The file %1 has been modified.
Do you want to save your changes?</source>
        <translation>文件%1被修改了.
是否要保存修改后的文件?</translation>
    </message>
    <message>
        <source>Quick Print...</source>
        <translation>快速打印...</translation>
    </message>
    <message>
        <source>There is no defaullt printer, please set one before trying quick print</source>
        <translation>无法找到默认打印机,请在尝试快速打印之前设置一台</translation>
    </message>
    <message>
        <source>Go To Line...</source>
        <translation>跳转到...</translation>
    </message>
    <message>
        <source>Enter the line number you want to go:</source>
        <translation>输入要跳转到的目标行数:</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <source>Add to watch: </source>
        <oldsource>AddtoWatch: </oldsource>
        <translation>添加到变量查看:</translation>
    </message>
    <message>
        <source>Add Conditional Breakpoint</source>
        <translation>插入条件断点</translation>
    </message>
    <message>
        <source>Delete All Breakpoint</source>
        <translation>删除全部断点</translation>
    </message>
    <message>
        <source>Condition:</source>
        <translation>条件:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <source>Recent files</source>
        <translation>最近的文件</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>编辑(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>查看(&amp;V)</translation>
    </message>
    <message>
        <source>&amp;Build</source>
        <translation>编连(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Debug</source>
        <translation>调试(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation>语言(&amp;L)</translation>
    </message>
    <message>
        <source>File Toolbar</source>
        <translation>文件</translation>
    </message>
    <message>
        <source>Edit Toolbar</source>
        <translation>编辑</translation>
    </message>
    <message>
        <source>Message</source>
        <translation>信息查看栏</translation>
    </message>
    <message>
        <source>Build</source>
        <translation>编译信息</translation>
    </message>
    <message>
        <source>Gdb Output</source>
        <translation>GDB信息</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation>堆栈信息</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>本地变量</translation>
    </message>
    <message>
        <source>Watch</source>
        <translation>变量查看</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation>内存查看</translation>
    </message>
    <message>
        <source>Address:</source>
        <translation>地址:</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>字节数:</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Build Toolbar</source>
        <translation>编连</translation>
    </message>
    <message>
        <source>Help Toolbar</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>Debug Toolbar</source>
        <translation>调试</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>新文件(&amp;N)</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>打开(&amp;O)...</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>保存文件(&amp;S)</translation>
    </message>
    <message>
        <source>Save &amp;As</source>
        <translation>另存为(&amp;A)</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>退出(&amp;x)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>剪切(&amp;t)</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>复制(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Close File</source>
        <translation>关闭文件(&amp;C)</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation>关闭全部文件</translation>
    </message>
    <message>
        <source>Close Other</source>
        <translation>除此文件之外全部关闭</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation>全选(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>撤销(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>重做(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Compile</source>
        <translation>编译(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Stop Compile</source>
        <translation>停止编译(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Execute</source>
        <translation>运行(&amp;E)</translation>
    </message>
    <message>
        <source>Step &amp;Into</source>
        <translation>逐语句(&amp;I)</translation>
    </message>
    <message>
        <source>Step O&amp;ver</source>
        <translation>逐过程(&amp;v)</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>结束(&amp;S)</translation>
    </message>
    <message>
        <source>Step &amp;Out</source>
        <translation>跳出(&amp;O)</translation>
    </message>
    <message>
        <source>Toggle &amp;Breakpoint</source>
        <translation>设置断点(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Search&amp;&amp;Replace</source>
        <translation>查找(&amp;S)/替换</translation>
    </message>
    <message>
        <source>&amp;Go To</source>
        <translation>跳转至(&amp;G)</translation>
    </message>
    <message>
        <source>Search Next</source>
        <translation>下一处</translation>
    </message>
    <message>
        <source>Search Previous</source>
        <translation>上一处</translation>
    </message>
    <message>
        <source>Stop Execute</source>
        <translation>停止运行</translation>
    </message>
    <message>
        <source>&amp;Help...</source>
        <translation>帮助(&amp;H)...</translation>
    </message>
    <message>
        <source>adb</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>Pause/Resume</source>
        <oldsource>PauseResume</oldsource>
        <translation>暂停</translation>
    </message>
    <message>
        <source>Run to Cursor</source>
        <translation>运行到光标处</translation>
    </message>
    <message>
        <source>English</source>
        <translation>英语</translation>
    </message>
    <message>
        <source>Reset extern tool</source>
        <translation>重设编译器路径</translation>
    </message>
    <message>
        <source>Go to &amp;Brace</source>
        <translation>跳转至匹配括号(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;File Option</source>
        <translation>文件选项(&amp;F)</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation>管理断点</translation>
    </message>
    <message>
        <source>One or more breakpoints are not positioned on valid lines.
These breakpoints will be moved to the next valid line.</source>
        <translation>存在设置无效的调试断点.
这些断点将会被移动至有效位置.</translation>
    </message>
    <message>
        <source>The source file has been modified, hence the following
information displayed may be incorrect.

Press Ok to continue debug anyway
Press Cancel to exit debug</source>
        <translation>由于源文件已经被修改过,这可能导致
调试信息显示异常.

点击OK按钮继续调试
点击Cancel退出调试</translation>
    </message>
    <message>
        <source>Compile source file...
Do you want to stop compiling file?</source>
        <translation>正在编译文件...
是否要停止编译过程?</translation>
    </message>
    <message>
        <source>Running target...
Do you want to stop running?</source>
        <translation>目标文件正在运行...
是否要停止运行?</translation>
    </message>
    <message>
        <source>Debugging target...
Do you want to stop debugging target?</source>
        <translation>正在调试目标文件...
是否要停止调试过程?</translation>
    </message>
    <message>
        <source>All Files</source>
        <translation>全部文件</translation>
    </message>
    <message>
        <source>Choose a file to open</source>
        <translation>请选择要打开的文件</translation>
    </message>
    <message>
        <source>C/C++ Sources</source>
        <translation>C/C++程序</translation>
    </message>
    <message>
        <source>Pascal Sources</source>
        <translation>Pascal程序</translation>
    </message>
    <message>
        <source>Texts</source>
        <translation>文本文件</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>关于 %1</translation>
    </message>
    <message>
        <source>&lt;p style=&quot;text-align: center;&quot;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;&lt;br&gt;&lt;br&gt;&lt;b&gt;Version:&lt;/b&gt;%2&lt;br&gt;&lt;b&gt;Author:&lt;/b&gt;%3&lt;br&gt;&lt;b&gt;Home:&lt;/b&gt;&lt;a href=&quot;%4&quot;&gt;%5&lt;/a&gt;&lt;br&gt;&lt;p style=&quot;text-align: center;&quot;&gt;%6&lt;/p&gt;</source>
        <translation>&lt;p style=&quot;text-align: center;&quot;&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;&lt;br&gt;&lt;br&gt;&lt;b&gt;版本:&lt;/b&gt;%2&lt;br&gt;&lt;b&gt;作者:&lt;/b&gt;%3&lt;br&gt;&lt;b&gt;主页:&lt;/b&gt;&lt;a href=&quot;%4&quot;&gt;%5&lt;/a&gt;&lt;br&gt;&lt;p style=&quot;text-align: center;&quot;&gt;%6&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Create a new file</source>
        <translation>创建一个新文件</translation>
    </message>
    <message>
        <source>Open an existing file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <source>Save the document to disk</source>
        <translation>保存当前文件</translation>
    </message>
    <message>
        <source>Save the document under a new name</source>
        <translation>当前文件另存为</translation>
    </message>
    <message>
        <source>Close current file</source>
        <translation>关闭当前文件</translation>
    </message>
    <message>
        <source>Close all files</source>
        <translation>关闭所有文件</translation>
    </message>
    <message>
        <source>Close other files except current one</source>
        <translation>除此文件之外全部关闭</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation>退出GUIDE</translation>
    </message>
    <message>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>剪切选中的内容</translation>
    </message>
    <message>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>复制选中的内容</translation>
    </message>
    <message>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>将剪贴板中的内容粘贴到光标处</translation>
    </message>
    <message>
        <source>Select all contents in current editor</source>
        <translation>全部选中所有内容</translation>
    </message>
    <message>
        <source>Undo last change</source>
        <translation>撤销</translation>
    </message>
    <message>
        <source>Redo last canceled change</source>
        <translation>恢复</translation>
    </message>
    <message>
        <source>Search in the file</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Search next result</source>
        <translation>下一处</translation>
    </message>
    <message>
        <source>Search previous result</source>
        <translation>上一处</translation>
    </message>
    <message>
        <source>Go to line in the file</source>
        <translation>跳转到指定行</translation>
    </message>
    <message>
        <source>Go to the position of the corresponding brace.</source>
        <translation>跳转至匹配的括号处.</translation>
    </message>
    <message>
        <source>Toggle breakpoint</source>
        <translation>设置断点</translation>
    </message>
    <message>
        <source>Manage all breakpoints</source>
        <translation>断点管理</translation>
    </message>
    <message>
        <source>Reset extern tools</source>
        <translation>重设编译器路径</translation>
    </message>
    <message>
        <source>Compile current source.</source>
        <translation>编译当前文件.</translation>
    </message>
    <message>
        <source>Stop current compiling process.</source>
        <translation>停止编译过程.</translation>
    </message>
    <message>
        <source>Run current executable.</source>
        <translation>运行当前文件.</translation>
    </message>
    <message>
        <source>Stop current running target</source>
        <translation>停止运行当前文件</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>开始调试</translation>
    </message>
    <message>
        <source>Debug current executable.</source>
        <translation>调试当前文件.</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>结束调试</translation>
    </message>
    <message>
        <source>Stop debug</source>
        <translation>停止调试</translation>
    </message>
    <message>
        <source>File Option</source>
        <translation>文件设置</translation>
    </message>
    <message>
        <source>Set file option</source>
        <translation>文件设置</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>暂停调试</translation>
    </message>
    <message>
        <source>Pause debug</source>
        <translation>暂停调试</translation>
    </message>
    <message>
        <source>Step Over</source>
        <translation>逐过程</translation>
    </message>
    <message>
        <source>Run to cursor</source>
        <translation>运行到光标处</translation>
    </message>
    <message>
        <source>Step Into</source>
        <translation>逐语句</translation>
    </message>
    <message>
        <source>Step Out</source>
        <translation>跳出</translation>
    </message>
    <message>
        <source>Show the application&apos;s About box</source>
        <translation>显示关于</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation>中文</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>就序</translation>
    </message>
    <message>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>文件内容已经发生了修改.
是否要保存修改后的文件?</translation>
    </message>
    <message>
        <source>The source has no name and been modified.
Do you want to save your changes?</source>
        <translation>当前文件没有指定文件名而且已经发生过修改.
是否要保存修改后的文件?</translation>
    </message>
    <message>
        <source>This file can not be compiled!
</source>
        <translation>当前文件无法编译!</translation>
    </message>
    <message>
        <source>The source has been modified.
Do you want to save your changes and compile the source?</source>
        <translation>文件内容已经发生了修改.
是否要保存修改后文件并进行编译?</translation>
    </message>
    <message>
        <source>Build finished successfully.</source>
        <translation>编译成功.</translation>
    </message>
    <message>
        <source>Build finished with</source>
        <translation>编译结束但存在</translation>
    </message>
    <message>
        <source>error(s)</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>and</source>
        <translation>和</translation>
    </message>
    <message>
        <source>warning(s)</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>Compile failed!
Can not execute the target!</source>
        <translation>编译失败!
无法运行目标文件!</translation>
    </message>
    <message>
        <source>The program exit with code %1 normally.</source>
        <translation>程序返回代码%1正常退出.</translation>
    </message>
    <message>
        <source>The program has crashed.</source>
        <translation>程序崩溃.</translation>
    </message>
    <message>
        <source>The program can not start!</source>
        <translation>程序无法启动!</translation>
    </message>
    <message>
        <source>Compile failed!
Can not debug the target!</source>
        <translation>编译失败!
无法调试目标程序!</translation>
    </message>
    <message>
        <source>The document can not be debugged.</source>
        <translation>目标程序无法被调试.</translation>
    </message>
    <message>
        <source>Can not launch gdb!</source>
        <translation>GDB调试器配置异常,无法启动调试!</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <source>Pause is impossible under Windows. Use breakpoints.</source>
        <translation>在WINDOWS系统下,该功能不可用.请尝试手工设置断点.</translation>
    </message>
    <message>
        <source>An segment error occured!Please stop the debug process and check your source code.</source>
        <translation>程序发生内存非法访问错误!请停止调试过程并重新检查您的源程序.</translation>
    </message>
    <message>
        <source>The variable &quot;%1&quot;
 already exists.</source>
        <translation>变量&quot;%1&quot;
 已经存在.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>New Watch:</source>
        <translation>新变量查看:</translation>
    </message>
    <message>
        <source>Target file has been renamed as %1
</source>
        <translation>目标文件已被改名为%1</translation>
    </message>
    <message>
        <source>Can not find g++!</source>
        <translation>无法找到g++!</translation>
    </message>
    <message>
        <source>Can not find gcc!</source>
        <translation>无法找到gcc!</translation>
    </message>
    <message>
        <source>Can not find fpc!</source>
        <translation>无法找到fpc!</translation>
    </message>
    <message>
        <source>File Browser</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <source>GUIDE can&apos;t step out of the main function!</source>
        <translation>GUIDE无法跳出main顶层函数!</translation>
    </message>
    <message>
        <source>&amp;%1 %2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Name</source>
        <translation>变量名</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <source>Unable to read memory</source>
        <translation>无法读取内存</translation>
    </message>
    <message>
        <source>Neither address nor length could be empty</source>
        <translation>地址和字节数都不能为空</translation>
    </message>
    <message>
        <source>The length cannot be zero</source>
        <translation>字节数不能为0</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation>装载中...</translation>
    </message>
    <message>
        <source>Checking external tools...</source>
        <translation>正在检查外部工具...</translation>
    </message>
    <message>
        <source>Main Window creation</source>
        <translation>创建主窗口</translation>
    </message>
    <message>
        <source>An error occured when loading api file:
%1</source>
        <translation>装载API文件时发生了错误:
%1</translation>
    </message>
</context>
<context>
    <name>RegistersWatch</name>
    <message>
        <source>Register Watch</source>
        <translation>寄存器查看</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>变量名</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
</context>
<context>
    <name>SettingDialog</name>
    <message>
        <source>Settings</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>General</source>
        <translation>总体</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation>编辑器</translation>
    </message>
    <message>
        <source>Auto Completion</source>
        <translation>自动补全</translation>
    </message>
    <message>
        <source>CallTips</source>
        <translation>提示显示设置</translation>
    </message>
    <message>
        <source>Indentation</source>
        <translation>缩进</translation>
    </message>
    <message>
        <source>Brace Matching</source>
        <translation>括号匹配</translation>
    </message>
    <message>
        <source>Edge</source>
        <translation>代码行长</translation>
    </message>
    <message>
        <source>Caret</source>
        <translation>光标行</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>代码折叠</translation>
    </message>
    <message>
        <source>Eol &amp; Wrap Mode</source>
        <translation>行尾及自动换行模式</translation>
    </message>
    <message>
        <source>Source APIs</source>
        <translation>文件APIs</translation>
    </message>
    <message>
        <source>Lexers Associations</source>
        <translation>词法分析器关联</translation>
    </message>
    <message>
        <source>Lexers Highlighting</source>
        <translation>语法高亮显示</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>Automatic Syntax Check</source>
        <translation>自动语法检查</translation>
    </message>
    <message>
        <source>Convert Tabs Upon Open</source>
        <translation>在打开之前转换Tab键</translation>
    </message>
    <message>
        <source>Create Backup File Upon Open</source>
        <translation>在打开之前备份文件</translation>
    </message>
    <message>
        <source>Automatic End of Line Conversion</source>
        <translation>自动换行符转换</translation>
    </message>
    <message>
        <source>Default encoding</source>
        <translation>默认编码</translation>
    </message>
    <message>
        <source>Selection Colours</source>
        <translation>颜色选择</translation>
    </message>
    <message>
        <source>Background :</source>
        <translation>背景色:</translation>
    </message>
    <message>
        <source>Foreground :</source>
        <translation>前景色:</translation>
    </message>
    <message>
        <source>Default Document Colours</source>
        <translation>默认文档颜色</translation>
    </message>
    <message>
        <source>Pen :</source>
        <translation>文字:</translation>
    </message>
    <message>
        <source>Paper :</source>
        <translation>背景:</translation>
    </message>
    <message>
        <source>Auto Completion Enabled</source>
        <translation>启用自动完成</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>源文件</translation>
    </message>
    <message>
        <source>from Document</source>
        <translation>来自文件</translation>
    </message>
    <message>
        <source>from API file</source>
        <translation>来自API文件</translation>
    </message>
    <message>
        <source>from All</source>
        <translation>来自所有</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation>区分大小写</translation>
    </message>
    <message>
        <source>Replace Word</source>
        <translation>替换单词</translation>
    </message>
    <message>
        <source>Show Single</source>
        <translation>单一显示</translation>
    </message>
    <message>
        <source>Threshold :</source>
        <translation>补全开端长:</translation>
    </message>
    <message>
        <source>Calltips Enabled</source>
        <translation>启用自动提示</translation>
    </message>
    <message>
        <source>Visible Calltips :</source>
        <translation>可见提示的长度:</translation>
    </message>
    <message>
        <source>No Context</source>
        <translation>无上下文</translation>
    </message>
    <message>
        <source>Context </source>
        <translation>上下文</translation>
    </message>
    <message>
        <source>No Auto Completion Context</source>
        <translation>无上下文自动补齐</translation>
    </message>
    <message>
        <source>Highlight :</source>
        <translation>高亮:</translation>
    </message>
    <message>
        <source>Indentation Width :</source>
        <translation>自动缩进宽度:</translation>
    </message>
    <message>
        <source>Tab Width :</source>
        <translation>Tab键宽度:</translation>
    </message>
    <message>
        <source>Indentation Guides Colours :</source>
        <translation>自动缩进格式符颜色:</translation>
    </message>
    <message>
        <source>Backspace Unindents</source>
        <translation>Backspace减少缩进</translation>
    </message>
    <message>
        <source>Tab Indents</source>
        <translation>Tab增回缩进</translation>
    </message>
    <message>
        <source>Indentation Guides</source>
        <translation>自动缩进先行符颜色</translation>
    </message>
    <message>
        <source>Auto Indent</source>
        <translation>自动缩进</translation>
    </message>
    <message>
        <source>Indentation use Tabs</source>
        <translation>使用Tab键自动缩进</translation>
    </message>
    <message>
        <source>Brace Matching Enabled</source>
        <translation>启用括号匹配</translation>
    </message>
    <message>
        <source>Unmatched Brace Foreground :</source>
        <translation>未匹配括号前景色:</translation>
    </message>
    <message>
        <source>Matched Brace Background :</source>
        <translation>匹配括号的背景色:</translation>
    </message>
    <message>
        <source>Unmatched Brace Background :</source>
        <translation>未匹配括号背景色:</translation>
    </message>
    <message>
        <source>Matched Brace Foreground :</source>
        <translation>匹配括号前景色:</translation>
    </message>
    <message>
        <source>Strict Brace Match</source>
        <translation>严格括号匹配</translation>
    </message>
    <message>
        <source>Sloppy Brace Match</source>
        <translation>随意括号匹配</translation>
    </message>
    <message>
        <source>Edge Mode Enabled</source>
        <translation>启用行长设置</translation>
    </message>
    <message>
        <source>Color :</source>
        <translation>颜色:</translation>
    </message>
    <message>
        <source>Column Number :</source>
        <translation>栏号码:</translation>
    </message>
    <message>
        <source>Edge Line</source>
        <translation>宽度边缘线</translation>
    </message>
    <message>
        <source>Edge Background</source>
        <translation>宽度外着色</translation>
    </message>
    <message>
        <source>Caret Line Visible</source>
        <translation>光标行高亮显示</translation>
    </message>
    <message>
        <source>Caret Line Background :</source>
        <translation>光标行背景:</translation>
    </message>
    <message>
        <source>Caret Foreground :</source>
        <translation>光标行前景色:</translation>
    </message>
    <message>
        <source>Width :</source>
        <translation>宽度:</translation>
    </message>
    <message>
        <source>Line Numbers Margin Enabled</source>
        <translation>启用行码空白</translation>
    </message>
    <message>
        <source>Auto Width</source>
        <translation>自动宽度</translation>
    </message>
    <message>
        <source>Fold Margin Enabled</source>
        <translation>启动折叠空白</translation>
    </message>
    <message>
        <source>Plain Fold Style</source>
        <translation>简单折叠风格</translation>
    </message>
    <message>
        <source>Circled Tree Fold Style</source>
        <translation>环绕树折叠风格</translation>
    </message>
    <message>
        <source>Circled Fold Style</source>
        <translation>环绕折叠风格</translation>
    </message>
    <message>
        <source>Boxed Fold Style</source>
        <translation>盒折叠风格</translation>
    </message>
    <message>
        <source>Boxed Tree Fold Style</source>
        <translation>盒树折叠风格</translation>
    </message>
    <message>
        <source>Global Margins Enabled</source>
        <translation>启用全局页边设置</translation>
    </message>
    <message>
        <source>Set fold comments</source>
        <translation>设置折叠注释</translation>
    </message>
    <message>
        <source>Set fold compact</source>
        <translation>设置紧凑折叠</translation>
    </message>
    <message>
        <source>Set fold quotes</source>
        <translation>设置折叠引号</translation>
    </message>
    <message>
        <source>Set fold directives</source>
        <translation>设置折叠指令</translation>
    </message>
    <message>
        <source>Set fold at begin</source>
        <translation>设置折叠于行首</translation>
    </message>
    <message>
        <source>Set fold at parenthesis</source>
        <translation>设置折叠插入语</translation>
    </message>
    <message>
        <source>Set fold at else</source>
        <translation>设置在ELSE处折叠</translation>
    </message>
    <message>
        <source>Set fold preprocessor</source>
        <translation>设置折叠预处理</translation>
    </message>
    <message>
        <source>Set style preprocessor</source>
        <translation>设置风格预处理</translation>
    </message>
    <message>
        <source>Set case sensitive tags</source>
        <translation>设置大小写敏感标识</translation>
    </message>
    <message>
        <source>Set backslash escapes</source>
        <translation>设置反斜线转义</translation>
    </message>
    <message>
        <source>Set indentation warning</source>
        <translation>设置缩进警告</translation>
    </message>
    <message>
        <source>Font :</source>
        <translation>字体:</translation>
    </message>
    <message>
        <source>Eol Mode</source>
        <translation>行末模式</translation>
    </message>
    <message>
        <source>Eol Visibility</source>
        <translation>换行符可见</translation>
    </message>
    <message>
        <source>Whitespace Visibility Enabled</source>
        <translation>空格符可见</translation>
    </message>
    <message>
        <source>Visible</source>
        <translation>可见</translation>
    </message>
    <message>
        <source>Visible After Indent</source>
        <translation>缩进时可见</translation>
    </message>
    <message>
        <source>Wrap Mode Enabled</source>
        <translation>启用换行模式</translation>
    </message>
    <message>
        <source>Wrap Word</source>
        <translation>单词换行</translation>
    </message>
    <message>
        <source>Wrap Character</source>
        <translation>字符换行</translation>
    </message>
    <message>
        <source>Wrap Visual Flags Enabled</source>
        <translation>启用可见自动换行符</translation>
    </message>
    <message>
        <source>Indent Width :</source>
        <translation>缩进宽度:</translation>
    </message>
    <message>
        <source>Flag By Text</source>
        <translation>靠近文字</translation>
    </message>
    <message>
        <source>Flag By Border</source>
        <translation>靠近边缘</translation>
    </message>
    <message>
        <source>Start :</source>
        <translation>行首:</translation>
    </message>
    <message>
        <source>End :</source>
        <translation>行尾:</translation>
    </message>
    <message>
        <source>APIs</source>
        <translation>文件APIs</translation>
    </message>
    <message>
        <source>List of API files</source>
        <translation>API文件列表</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Language :</source>
        <translation>程序语言:</translation>
    </message>
    <message>
        <source>Select language to be configured.</source>
        <translation>选择要配置的程序语言.</translation>
    </message>
    <message>
        <source>Filename Pattern</source>
        <translation>文件扩展名</translation>
    </message>
    <message>
        <source>Lexer Language</source>
        <translation>语法分析器所使用的语言</translation>
    </message>
    <message>
        <source>Filename Pattern :</source>
        <translation>文件扩展名:</translation>
    </message>
    <message>
        <source>Add/Change</source>
        <translation>添加或修改</translation>
    </message>
    <message>
        <source>Style Element</source>
        <translation>风格元素</translation>
    </message>
    <message>
        <source>Foreground Colour</source>
        <translation>前景色</translation>
    </message>
    <message>
        <source>Background Colour</source>
        <translation>背景色</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <source>Fill to end of line</source>
        <translation>添充行尾</translation>
    </message>
    <message>
        <source>Fold comments</source>
        <translation>折叠注释</translation>
    </message>
    <message>
        <source>Fold compact</source>
        <translation>紧凑折叠</translation>
    </message>
    <message>
        <source>Fold quotes</source>
        <translation>折叠引号</translation>
    </message>
    <message>
        <source>Fold directives</source>
        <translation>折叠指令</translation>
    </message>
    <message>
        <source>Fold at begin</source>
        <translation>折叠于行首</translation>
    </message>
    <message>
        <source>Fold at parenthesis</source>
        <translation>折叠插入语</translation>
    </message>
    <message>
        <source>Fold at else</source>
        <translation>在ELSE处折叠</translation>
    </message>
    <message>
        <source>Fold preprocessor</source>
        <translation>折叠预处理</translation>
    </message>
    <message>
        <source>Style preprocessor</source>
        <translation>风格预处理</translation>
    </message>
    <message>
        <source>Indent opening brace</source>
        <translation>缩进打开括号</translation>
    </message>
    <message>
        <source>Indent closing brace</source>
        <translation>缩进关闭括号</translation>
    </message>
    <message>
        <source>Case sensitive tags</source>
        <translation>大小写敏感标识</translation>
    </message>
    <message>
        <source>Backslash escapes</source>
        <translation>反斜线转义</translation>
    </message>
    <message>
        <source>Indentation warning :</source>
        <translation>缩进警告:</translation>
    </message>
    <message>
        <source>All Foreground Colours</source>
        <translation>所有前景色</translation>
    </message>
    <message>
        <source>All Background Colours</source>
        <translation>所有背景色</translation>
    </message>
    <message>
        <source>All Fonts</source>
        <translation>全部字体</translation>
    </message>
    <message>
        <source>Reset Current Lexer</source>
        <translation>重设当前语法分析器</translation>
    </message>
    <message>
        <source>Lexer Language :</source>
        <translation>语法分析器语言:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>No warning</source>
        <translation>不显示警告</translation>
    </message>
    <message>
        <source>Inconsistent</source>
        <translation>不一致</translation>
    </message>
    <message>
        <source>Tabs after spaces</source>
        <translation>由空格后跟着Tab键构成</translation>
    </message>
    <message>
        <source>Spaces</source>
        <translation>由空格构成</translation>
    </message>
    <message>
        <source>Tabs</source>
        <translation>由Tab键构成</translation>
    </message>
    <message>
        <source>Select API file</source>
        <translation>选择API文件</translation>
    </message>
    <message>
        <source>API Files (*.api);;All Files (*)</source>
        <translation>API 文件(*.api);;所有文件 (*)</translation>
    </message>
    <message>
        <source>Windows/DOS</source>
        <translation></translation>
    </message>
    <message>
        <source>Macintosh</source>
        <translation></translation>
    </message>
    <message>
        <source>Unix</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>bkptsDlg</name>
    <message>
        <source>Breakpoints Manager</source>
        <translation>断点管理</translation>
    </message>
    <message>
        <source>Condition</source>
        <translation>条件</translation>
    </message>
    <message>
        <source>Line</source>
        <translation>行号</translation>
    </message>
    <message>
        <source>Delete selected breakpoints</source>
        <translation>删除选定断点</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Select all breakpoints</source>
        <translation></translation>
    </message>
    <message>
        <source>All</source>
        <translation></translation>
    </message>
    <message>
        <source>Invert selection</source>
        <translation></translation>
    </message>
    <message>
        <source>Invert</source>
        <translation></translation>
    </message>
    <message>
        <source>Confirm change of conditions</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>setting_startnum</name>
    <message>
        <source>Setting start number</source>
        <translation>设置起始点数字</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;The array is too large to expand all!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GUIDE will show no more than 500 items of your array.Please input the starting number and the length that need to show.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;The array is too large to expand all!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GUIDE will show no more than 500 items of your array.Please input the starting number and the length that need to show.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;宋体&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;当前数组长度过大!GUIDE将会显示其中不多于500项的内容.请选择起始元素的下标和显示部分的元素个数.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Starting Point</source>
        <translation>起始点的数字</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>显示长度</translation>
    </message>
</context>
</TS>
