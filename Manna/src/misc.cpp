#include "misc.h"
#include <QDir>

#ifdef Q_OS_WIN
#include <shlobj.h>
#endif

QString getMannaPath()
{
    static QString path;
    if (!path.isEmpty())
        return path;

#ifdef Q_OS_WIN
    wchar_t buf[MAX_PATH];
    if (!SHGetFolderPathW(NULL, CSIDL_APPDATA, NULL, 0, buf))
        path = QString::fromUtf16((ushort *)buf)+"/";
    else
        path = QDir::homePath()+"/Application Data/"; // this shouldn't happen
#else
    path = QDir::homePath()+"/";
#endif

    // create subdir
    QDir dir(path);
#ifdef Q_OS_WIN
    dir.mkdir("Manna");
    path += "Manna/";
#else
    dir.mkdir(".Manna");
    path += ".Manna/";
#endif
    return path;
}

QString getMannaSettingFile()
{
    return getMannaPath() + "Manna.ini";
}
#ifndef Q_OS_WIN
void setLocaleToC()
{
    setenv("LANG", "C", 1);
    setenv("LC_ALL", "C", 1);
}
#endif
