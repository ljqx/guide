#include "set_startnum.h"
#include <QDebug>

Set_startnum::Set_startnum(int &cur_pos, int array_length,int &show_length)
{
    setupUi(this);
    this->set_num = & cur_pos;
    this->arraylength = & array_length;
    this->showlength = & show_length;

    //arraylength = array_length;
    this->spinBox->setMaximum(array_length);
    this->spinBox->setMinimum(0);
    this->spinBox->setValue(cur_pos);

    this->spinBox_length->setValue(show_length);

    connect(this->pushButton,SIGNAL(pressed()),this,SLOT(slotsOK()));
    connect(this->pushButton_2,SIGNAL(pressed()),this,SLOT(slotsCancel()));
    this->pushButton_2->setVisible(false);

    this->label_3->setText(QString("(%1-%2)").arg(*set_num).arg(*arraylength-*showlength));

    connect(this->horizontalSlider,SIGNAL(valueChanged(int)),this,SLOT(slotSliderValueChanged(int)));
    this->horizontalSlider->setMaximum(array_length);
    this->horizontalSlider->setMinimum(0);
    this->horizontalSlider->setValue(cur_pos);
    this->horizontalSlider->setVisible(false);
    connect(this->spinBox,SIGNAL(valueChanged(int)),this,SLOT(slotSpinBoxValueChanged(int)));
    connect(this->spinBox_length,SIGNAL(valueChanged(int)),this,SLOT(slotSpinBoxLengthValueChanged(int)));
}

void Set_startnum::slotSpinBoxLengthValueChanged(int value)
{
    *showlength = this->spinBox_length->value();
    this->label_3->setText(QString("(%1-%2)").arg(*set_num).arg(*arraylength-*showlength));
}

void Set_startnum::slotsOK()
{
    *set_num = this->spinBox->value();
    *showlength = this->spinBox_length->value();
    this->close();
}

void Set_startnum::slotsCancel()
{
    this->close();
}

void Set_startnum::slotSliderValueChanged(int value)
{
    this->spinBox->setValue(value);
}

void Set_startnum::slotSpinBoxValueChanged(int value)
{
    this->horizontalSlider->setValue(value);
}
