#include "filebrowser.h"

#include <QDirModel>

#include "treecombobox.h"
#include "mannasettings.h"

bool FileBrowser::FilteredModel::filterAcceptsRow( int row, const QModelIndex& parent ) const
{
    if ( parent == QModelIndex() )
        return true;
    foreach ( const QString s, m_Wildcards )
        if ( QRegExp( s, Qt::CaseSensitive, QRegExp::Wildcard ).exactMatch( parent.child( row, 0 ).data().toString() ) )
                return false;
    return true;
}

FileBrowser::FileBrowser( QWidget* parent )
    : QWidget( parent ), m_Shown( false )
{
    setupUi(this);
    // dir model
    m_DirsModel = new QDirModel( this );
    m_DirsModel->setFilter( QDir::AllEntries | QDir::Readable | QDir::CaseSensitive | QDir::NoDotAndDotDot );
    m_DirsModel->setSorting( QDir::DirsFirst | QDir::Name );

    m_FilteredModel = new FilteredModel ( this );
    m_FilteredModel->setSourceModel( m_DirsModel );

    // assign model to views
    mCombo->setModel( m_DirsModel );
    mTree->setModel( m_FilteredModel );

    // custom view
    mCombo->view()->setColumnHidden( 1, true );
    mCombo->view()->setColumnHidden( 2, true );
    mCombo->view()->setColumnHidden( 3, true );
    mTree->setColumnHidden( 1, true );
    mTree->setColumnHidden( 2, true );
    mTree->setColumnHidden( 3, true );

    // set root index
#ifndef Q_OS_WIN
    char *home=getenv("HOME");
    if (!home)
        home = "/";
    mCombo->setRootIndex( m_DirsModel->index( home ) );
#else
    mCombo->setRootIndex( QModelIndex() );
#endif

    // set lineedit path
    MannaSettings *settings = MannaSettings::getMannaSettings();
    settings->beginGroup("LastestBrowserDir");
    QString lastest = settings->value( "LastestDir",m_DirsModel->filePath( mCombo->rootIndex() ) ).toString();
    settings->endGroup();
    setCurrentPath( lastest );

    // restrict areas

    // connections
    connect( tbUp, SIGNAL( clicked() ), this, SLOT( up() ) );
    connect( tbRefresh, SIGNAL( clicked() ), this, SLOT( refresh() ) );
    connect( tbRoot, SIGNAL( clicked() ), this, SLOT( root() ) );
    connect( mCombo, SIGNAL( currentChanged( const QModelIndex& ) ), this, SLOT( cb_currentChanged( const QModelIndex& ) ) );
    connect( mTree, SIGNAL( doubleClicked( const QModelIndex& ) ), this, SLOT( tv_doubleClicked( const QModelIndex& ) ) );
}

void FileBrowser::up()
{
    // cd up only if not the root index
    if ( mCombo->currentIndex() != mCombo->rootIndex() )
        setCurrentPath( m_DirsModel->filePath( mCombo->currentIndex().parent() ) );
}

void FileBrowser::refresh()
{
    // refresh current parent folder
    m_DirsModel->refresh( mCombo->currentIndex() );
}

void FileBrowser::root()
{
    // seet root of model to path of selected item
    QModelIndex index = mTree->selectionModel()->selectedIndexes().value( 0 );
    if ( !index.isValid() )
        return;
    index = m_FilteredModel->mapToSource( index );
    if ( !m_DirsModel->isDir( index ) )
        index = index.parent();
    setCurrentPath( m_DirsModel->filePath( index ) );
}

void FileBrowser::tv_doubleClicked( const QModelIndex& i )
{
    // open file corresponding to index
    QModelIndex index = m_FilteredModel->mapToSource( i );

    if ( !m_DirsModel->isDir( index ) )
        emit fileDblClicked(m_DirsModel->filePath( index ));
}

void FileBrowser::cb_currentChanged( const QModelIndex& i )
{ setCurrentPath( m_DirsModel->filePath( i ) ); }

QString FileBrowser::currentPath() const
{ return m_DirsModel->filePath( mCombo->currentIndex() ); }

void FileBrowser::setCurrentPath( const QString& s )
{
    // get index
    QModelIndex index = m_DirsModel->index( s );
    // set current path
    mCombo->setCurrentIndex( index );
    m_FilteredModel->invalidate();
    mTree->setRootIndex( m_FilteredModel->mapFromSource( index ) );
    // set lineedit path
    mLineEdit->setText( m_DirsModel->filePath( index ) );
    mLineEdit->setToolTip( mLineEdit->text() );

    MannaSettings *settings = MannaSettings::getMannaSettings();
    settings->beginGroup("LastestBrowserDir");
    settings->setValue("LastestDir",s);
    settings->endGroup();
}

QStringList FileBrowser::wildcards() const
{ return m_FilteredModel->m_Wildcards; }

void FileBrowser::setWildcards( const QStringList& l )
{
    const QString s = currentPath();
    m_FilteredModel->m_Wildcards = l;
    m_FilteredModel->invalidate();
    setCurrentPath( s );
}

/*
void FileBrowser::reCreateStatusTips()
{
    //because the way refresh ui of browser need to change a lot of the file.I didn't have a better way now.
    // to avoid cause big trouble.I decide not to fix this bug temporarily.
}
*/
void FileBrowser::reCreateStatusTips()
{
}
