#include "eventhandler.h"

#include "mainwindow.h"

MEventHandler::MEventHandler (MainWindow * mainWindow) : m_mainWindow (mainWindow)
{
    m_mainWindow->installEventFilter (this);
}

bool MEventHandler::eventFilter (QObject * obj, QEvent * event)
{
    if (event->type () == ME_TARGET_QUIT)
    {
        //m_mainWindow->stopDebug ();
        return true;
    } else {
        return QObject::eventFilter (obj, event);
    }
} 
