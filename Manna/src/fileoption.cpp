#include "fileoption.h"
#include "mannasettings.h"

void FileOption::load(const QString &group)
{
    MannaSettings *ss = MannaSettings::getMannaSettings();

    ss->beginGroup(group);
    m_compileOpt = ss->value("CompileOpt").toString();
    m_execOpt = ss->value("ExecOpt").toString();
    QMap<QString, QVariant> bk;
    m_breakPts.clear();
    bk = ss->value("BreakPoints").toMap();
    QMap<QString, QVariant>::iterator i;
    for (i = bk.begin(); i != bk.end(); ++i) {
        m_breakPts.insert(i.key().toInt(), i.value().toString());
    }
    ss->endGroup();
}

void FileOption::save(const QString &group)
{
    MannaSettings *ss = MannaSettings::getMannaSettings();

    ss->beginGroup(group);
    if (!m_compileOpt.isEmpty())
        ss->setValue("CompileOpt", m_compileOpt);
    if (!m_execOpt.isEmpty())
        ss->setValue("ExecOpt", m_execOpt);
    if (!m_breakPts.isEmpty()) {
        QMap<QString, QVariant> bk;
        QMap<int, QString>::iterator i;
        for (i = m_breakPts.begin(); i != m_breakPts.end(); ++i) {
            bk.insert(QString::number(i.key()), i.value());
        }
        ss->setValue("BreakPoints", bk);
    }
    ss->endGroup();
}
