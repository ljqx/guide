#include "fileoptiondlg.h"

FileOptionDlg::FileOptionDlg(QWidget * parent, Qt::WindowFlags f)
    : QDialog(parent, f)
{
    setupUi(this);
}

void FileOptionDlg::setCompileOption(QString &opt)
{
    leCompileOption->setText(opt);
}

QString FileOptionDlg::getCompileOption()
{
    return leCompileOption->text();
}

void FileOptionDlg::setCompileCmd(QString &cmd)
{
    labCompileCmd->setText (cmd);
}

void FileOptionDlg::setExecOption(QString &opt)
{
    leExecOption->setText (opt);
}

QString FileOptionDlg::getExecOption()
{
    return leExecOption->text();
}
