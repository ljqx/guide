#include <QtGui>
#include <QtAlgorithms>
#include <QTreeWidgetItem>
#include "varhelper.h"
#include "gdbmanager.h"

#include "set_startnum.h"

#define ARRAY_EAGE 500
static int g_show_length = ARRAY_EAGE;

MIVariable::~MIVariable ()
{
    removeAllChild ();
}

void MIVariable::removeAllChild ()
{
    qDeleteAll (m_children.begin (), m_children.end ());
    m_children.clear ();
}

MIVariable * MIVariable::findVariable (const QString & name)
{
    //TRACEFUNC(findVariable);
    QStringList nameList = name.split ('.');
    MIVariable * var = this;
    foreach (const QString & name, nameList) {
        MIVariable * result = 0;
        for (MIVariable::VARLIST::iterator it = var->m_children.begin ();
            it != var->m_children.end ();
            ++ it) {
            if ((* it)->m_name.section ('.', -1) == name) {
                result = * it;
                break;
            }
        }
        if (! result) {
            //qDebug ("failed to find %s", name.toLatin1 ().constData ());
            return 0;
        }
        var = result;
    }
    return var;
}

MIVariable * MIVariable::findVariable (const QString & name,bool /*isPascal*/)
{
    //TRACEFUNC(findVariable);
    QStringList nameList = name.split ('.');
    MIVariable * var = this;
    foreach (const QString & name, nameList) {
        MIVariable * result = 0;
        for (MIVariable::VARLIST::iterator it = var->m_children.begin ();
            it != var->m_children.end ();
            ++ it) {
            if (!QString::compare((*it)->m_name.section ('.', -1).toUpper(),name.toUpper())) {
                result = *it;
                break;
            }
        }
        if (! result) {
            //qDebug ("failed to find %s", name.toLatin1 ().constData ());
            return 0;
        }
        var = result;
    }
    return var;
}

void VarTreeManager::onItemExpanded (QTreeWidgetItem * item)
{
    TRACEFUNC(onItemExpanded);
    QVariant v = item->data (0, Qt::UserRole);
    MIVariable * var = v.value<MIVariable *> ();

    if (var && (var->status () & MIV_MORE)) {
        QRegExp rx ("\\[(\\d+)\\]");

        if (rx.indexIn (var->m_type) != -1 || var->m_childnum > ARRAY_EAGE) {
            ulong size = rx.cap (1).toULong ();
            if (var->m_childnum > size)
                size = var->m_childnum;
            if (size > ARRAY_EAGE || var->m_childnum > ARRAY_EAGE) {
                /*
                QMessageBox::warning (NULL, PROGRAM_NAME,
                    "This array is too large to expand",
                    QMessageBox::Ok);

                if (item->childCount ())
                {
                    QTreeWidgetItem * child = item->child (0);
                    child->setText (0, "this array is too large to expand");
                }
                return;*/
                //when the length of array is larger than 3200,
                int start_num = 0;

                Set_startnum * dlg = new Set_startnum(start_num,(int)size,g_show_length);
                dlg->exec();delete dlg;

                var->setStatus (MIV_EXPAND);
                //if (var->m_name.constains("GUIDE__")) {
                //    var->m_name = var->m_name(var->m_name.length() - 7);
                //}
                start_num = start_num * 1000 + g_show_length;
                m_client->updateVariableObject (var->m_name,! (var->status () & MIV_VAROBJ),start_num,(int)size);
                //qDebug(QString("%1--%2").arg(start_num).arg(size).toLatin1().constData());
                var->clearStatus (MIV_MORE);
                return;
            } else {
                var->setStatus (MIV_EXPAND);
                m_client->updateVariableObject (var->m_name,! (var->status () & MIV_VAROBJ));
                var->clearStatus (MIV_MORE);
            }
            return;
        }

        var->setStatus (MIV_EXPAND);
        m_client->updateVariableObject (var->m_name, ! (var->status () & MIV_VAROBJ));
        var->clearStatus (MIV_MORE);
    }
}

void VarTreeManager::onItemDoubleClicked (QTreeWidgetItem * item,int /*column*/)
{
    TRACEFUNC(onItemDoubleClicked);
    QVariant v = item->data (0, Qt::UserRole);
    MIVariable * var = v.value<MIVariable *> ();

    QRegExp rx ("\\[(\\d+)\\]");
    //qDebug(QString("(%1-%2").arg(var->status()).arg(MIV_EXPAND).toLatin1().constData());
    if (var && (var->status() & MIV_VAROBJ)) {
    //qDebug(")");
        if (rx.indexIn (var->m_type) != -1 || var->m_childnum > ARRAY_EAGE) {
            ulong size = rx.cap (1).toULong ();
            if (var->m_childnum > size)
                size = var->m_childnum;
            if (size > ARRAY_EAGE || var->m_childnum > ARRAY_EAGE) {
                //when the length of array is larger than 3200,
                int start_num = 0;
                bool ok;
                if (item->childCount() > 1) {
                    start_num = item->child(0)->text(0).toInt(&ok,10);
                } else {
                    start_num = 0;
                }
                Set_startnum * dlg = new Set_startnum(start_num,(int)size,g_show_length);
                dlg->exec();delete dlg;

                var->setStatus (MIV_EXPAND);
                start_num = start_num * 1000 + g_show_length;
                m_client->updateVariableObject (var->m_name,false,start_num,(int)size);
                qDebug(QString("%1--%2").arg(start_num).arg(size).toUtf8().constData());
                var->clearStatus (MIV_MORE);
                return;
            }
        }
    }
    //item->setExpanded(true);

    //if (var && (var->status () & MIV_MORE))
    //{
    //    if (rx.indexIn (var->m_type) != -1)
    //    {
    //        ulong size = rx.cap (1).toULong ();
    //        if (size > 100L)
    //        {
    //            int start_num = 0;
    //            bool ok;
    //            if (item->childCount() > 1) {
    //                ////qDebug(item->child(0)->text(0).toLatin1().constData());
    //                start_num = item->child(0)->text(0).toInt(&ok,10);
    //                ////qDebug(QString("***************%1").arg(start_num).toLatin1().constData());
    //            } else {
    //                start_num = 0;
    //            }
    //            item->takeChild(0);
    //            Set_startnum * dlg = new Set_startnum(start_num,(int)size);
    //            //dlg->setWindowModality(Qt::ApplicationModal);
    //            dlg->exec();delete dlg;
    //            ////qDebug(QString("***************%1").arg(start_num).toLatin1().constData());
    //            /*
    //            var->setStatus (MIV_EXPAND);
    //            m_client->updateVariableObject (var->m_name,! (var->status () & MIV_VAROBJ),0);
    //            var->clearStatus (MIV_MORE);
    //            return;*/
    //        }
    //    }
    //}
}
