#ifndef MANNASETTINGS_H
#define MANNASETTINGS_H

#include <QSettings>
#include <QHash>
#include <qscintilla.h>

class MannaSettings : public QSettings
{
    Q_OBJECT

public:
    MannaSettings( QObject* = 0 );
    ~MannaSettings();

    static void setIniInformations( const QString& = PROGRAM_NAME, const QString& = PROGRAM_VERSION );
    static MannaSettings *getMannaSettings();
    void prepareAPIs();

    QsciAPIs* apisForLexer( QsciLexer* l );
    QString languageForFileName( const QString& s );
    QsciLexer* lexerForFileName( const QString& s );
    QsciLexer* lexerForLanguage( const QString& s );

    void applySettings();

    /* Setting path */
    static const QString settingsPath();
    static const QString scintillaSettingsPath();

    void compilerPath(QString &, QString &, QString &, bool &);

protected:
    static QString m_ProgName;
    static QString m_ProgVersion;
    static MannaSettings *m_settings;
    static QHash<QString,QsciLexer*> m_lexers;
    static QHash<QString,QsciAPIs*> m_APIs;
};

class MEditor;
namespace optMannaSettings
{
    const QHash<QString, QStringList> defaultLanguagesSuffixes();
    const QHash<QString, QStringList> availableLanguagesSuffixes();
    const QHash<QString, QStringList> availableFilesSuffixes();
    const QStringList availableLanguages();
    const QStringList availableTextCodecs();
    const QStringList defaultAPIsForLanguages(const QString &ln);
    bool setLexerProperty( const QString&, QsciLexer*, const QVariant& );
    const QVariant lexerProperty( const QString&, QsciLexer* );
    void resetLexer( QsciLexer* l );
    QsciLexer * getLexerByLanguage(const QString &s);

    /* Editor settings */
    // General
    void setAutoSyntaxCheck( bool );
    //const bool autoSyntaxCheck();
    bool autoSyntaxCheck();
    void setConvertTabsUponOpen( bool );
    //const bool convertTabsUponOpen();
    bool convertTabsUponOpen();
    void setCreateBackupUponOpen( bool );
    //const bool createBackupUponOpen();
    bool createBackupUponOpen();
    void setAutoEolConversion( bool );
    //const bool autoEolConversion();
    bool autoEolConversion();
    void setDefaultEncoding( const QString& );
    const QString defaultEncoding();
    void setSelectionBackgroundColor( const QColor& );
    const QColor selectionBackgroundColor();
    void setSelectionForegroundColor( const QColor& );
    const QColor selectionForegroundColor();
    void setDefaultDocumentColours( bool );
    //const bool defaultDocumentColours();
    bool defaultDocumentColours();
    void setDefaultDocumentPen( const QColor& );
    const QColor defaultDocumentPen();
    void setDefaultDocumentPaper( const QColor& );
    const QColor defaultDocumentPaper();
    // Auto Completion
    void setAutoCompletionCaseSensitivity( bool );
    //const bool autoCompletionCaseSensitivity();
    bool autoCompletionCaseSensitivity();
    void setAutoCompletionReplaceWord( bool );
    //const bool autoCompletionReplaceWord();
    bool autoCompletionReplaceWord();
    void setAutoCompletionShowSingle( bool );
    //const bool autoCompletionShowSingle();
    bool autoCompletionShowSingle();
    void setAutoCompletionSource( QsciScintilla::AutoCompletionSource );
    //const QsciScintilla::AutoCompletionSource autoCompletionSource();
    QsciScintilla::AutoCompletionSource autoCompletionSource();
    void setAutoCompletionThreshold( int );
    //const int autoCompletionThreshold();
    int autoCompletionThreshold();
    // CallTips
    void setCallTipsBackgroundColor( const QColor& );
    const QColor callTipsBackgroundColor();
    void setCallTipsForegroundColor( const QColor& );
    const QColor callTipsForegroundColor();
    void setCallTipsHighlightColor( const QColor& );
    const QColor callTipsHighlightColor();
    void setCallTipsStyle( QsciScintilla::CallTipsStyle );
    //const QsciScintilla::CallTipsStyle callTipsStyle();
    QsciScintilla::CallTipsStyle callTipsStyle();
    void setCallTipsVisible( int );
    //const int callTipsVisible();
    int callTipsVisible();
    // Indentation
    void setAutoIndent( bool );
    //const bool autoIndent();
    bool autoIndent();
    void setBackspaceUnindents( bool );
    //const bool backspaceUnindents();
    bool backspaceUnindents();
    void setIndentationGuides( bool );
    //const bool indentationGuides();
    bool indentationGuides();
    void setIndentationsUseTabs( bool );
    //const bool indentationsUseTabs();
    bool indentationsUseTabs();
    void setIndentationWidth( int );
    //const int indentationWidth();
    int indentationWidth();
    void setTabIndents( bool );
    //const bool tabIndents();
    bool tabIndents();
    void setTabWidth( int );
    //const int tabWidth();
    int tabWidth();
    void setIndentationGuidesBackgroundColor( const QColor& );
    const QColor indentationGuidesBackgroundColor();
    void setIndentationGuidesForegroundColor( const QColor& );
    const QColor indentationGuidesForegroundColor();
    // Brace Matching
    void setBraceMatching( QsciScintilla::BraceMatch );
    //const QsciScintilla::BraceMatch braceMatching();
    QsciScintilla::BraceMatch braceMatching();
    void setMatchedBraceBackgroundColor( const QColor& );
    const QColor matchedBraceBackgroundColor();
    void setMatchedBraceForegroundColor( const QColor& );
    const QColor matchedBraceForegroundColor();
    void setUnmatchedBraceBackgroundColor( const QColor& );
    const QColor unmatchedBraceBackgroundColor();
    void setUnmatchedBraceForegroundColor( const QColor& );
    const QColor unmatchedBraceForegroundColor();
    // Edge Mode
    void setEdgeMode( QsciScintilla::EdgeMode );
    //const QsciScintilla::EdgeMode edgeMode();
    QsciScintilla::EdgeMode edgeMode();
    void setEdgeColor( const QColor& );
    const QColor edgeColor();
    void setEdgeColumn( int );
    //const int edgeColumn();
    int edgeColumn();
    // Caret
    void setCaretLineVisible( bool );
    //const bool caretLineVisible();
    bool caretLineVisible();
    void setCaretLineBackgroundColor( const QColor& );
    const QColor caretLineBackgroundColor();
    void setCaretForegroundColor( const QColor& );
    const QColor caretForegroundColor();
    void setCaretWidth( int );
    //const int caretWidth();
    int caretWidth();
    // Margins
    void setLineNumbersMarginEnabled( bool );
    //const bool lineNumbersMarginEnabled();
    bool lineNumbersMarginEnabled();
    void setLineNumbersMarginWidth( int );
    //const int lineNumbersMarginWidth();
    int lineNumbersMarginWidth();
    void setLineNumbersMarginAutoWidth( bool );
    //const bool lineNumbersMarginAutoWidth();
    bool lineNumbersMarginAutoWidth();
    void setFolding( QsciScintilla::FoldStyle );
    //const QsciScintilla::FoldStyle folding();
    QsciScintilla::FoldStyle folding();
    void setFoldMarginBackgroundColor( const QColor& );
    const QColor foldMarginBackgroundColor();
    void setFoldMarginForegroundColor( const QColor& );
    const QColor foldMarginForegroundColor();
    void setMarginsEnabled( bool );
    //const bool marginsEnabled();
    bool marginsEnabled();
    void setMarginsBackgroundColor( const QColor& );
    const QColor marginsBackgroundColor();
    void setMarginsForegroundColor( const QColor& );
    const QColor marginsForegroundColor();
    void setMarginsFont( const QFont& );
    const QFont marginsFont();
    // Special Characters
    void setEolMode( QsciScintilla::EolMode );
    //const QsciScintilla::EolMode eolMode();
    QsciScintilla::EolMode eolMode();
    QString getEol( QsciScintilla::EolMode = optMannaSettings::eolMode() );
    void setEolVisibility( bool );
    //const bool eolVisibility();
    bool eolVisibility();
    void setWhitespaceVisibility( QsciScintilla::WhitespaceVisibility );
    //const QsciScintilla::WhitespaceVisibility whitespaceVisibility();
    QsciScintilla::WhitespaceVisibility whitespaceVisibility();
    void setWrapMode( QsciScintilla::WrapMode );
    //const QsciScintilla::WrapMode wrapMode();
    QsciScintilla::WrapMode wrapMode();
    void setWrapVisualFlagsEnabled( bool );
    //const bool wrapVisualFlagsEnabled();
    bool wrapVisualFlagsEnabled();
    void setStartWrapVisualFlag( QsciScintilla::WrapVisualFlag );
    //const QsciScintilla::WrapVisualFlag startWrapVisualFlag();
    QsciScintilla::WrapVisualFlag startWrapVisualFlag();
    void setEndWrapVisualFlag( QsciScintilla::WrapVisualFlag );
    //const QsciScintilla::WrapVisualFlag endWrapVisualFlag();
    QsciScintilla::WrapVisualFlag endWrapVisualFlag();
    void setWrappedLineIndentWidth( int );
    //const int wrappedLineIndentWidth();
    int wrappedLineIndentWidth();

    /* Set editor */
    void setMEditorProperties( MEditor* e );

};
#endif //MANNASETTINGS_H
