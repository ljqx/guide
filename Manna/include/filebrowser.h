#ifndef FILEBROWSER_H
#define FILEBROWSER_H

#include <QSortFilterProxyModel>
#include <QDockWidget>
#include "ui_filebrowser.h"

class TreeComboBox;
class QLineEdit;
class QDirModel;
class QTreeView;
class QModelIndex;

class FileBrowser : public QWidget, public Ui::FileBrowser
{
    Q_OBJECT

    class FilteredModel: public QSortFilterProxyModel
    {
        friend class FileBrowser;

    protected:
        QStringList m_Wildcards;

        bool filterAcceptsRow( int row, const QModelIndex& parent ) const;

    public:
        FilteredModel( QObject* parent = 0 ) : QSortFilterProxyModel( parent ) {}
    };

public:
    FileBrowser( QWidget* = 0 );

    QString currentPath() const;
    QStringList wildcards() const;
    void reCreateStatusTips();

protected:
    bool m_Shown;
    QDirModel* m_DirsModel;
    FilteredModel* m_FilteredModel;

public slots:
    void setCurrentPath( const QString& );
    void setWildcards( const QStringList& );

private slots:
    void up();
    void refresh();
    void root();
    void cb_currentChanged( const QModelIndex& );
    void tv_doubleClicked( const QModelIndex& );

signals:
    void fileDblClicked(QString fname);
};

#endif // FILEBROWSER_H
