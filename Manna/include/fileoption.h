#ifndef FILEOPTION_H
#define FILEOPTION_H

#include <QString>
#include <QMap>

class FileOption
{
public:
    QString m_compileOpt;
    QString m_execOpt;
    QMap<int, QString> m_breakPts;

    void load(const QString &group);
    void save(const QString &group);
};
#endif //FILEOPTION_H
