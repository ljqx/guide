#ifndef MEDITOR_H
#define MEDITOR_H

#include <qscintilla.h>
#include "fileoption.h"

class QsciScintilla;
class EditorTabWidget;

class MEditor : public QsciScintilla
{
    Q_OBJECT

public:
    MEditor(EditorTabWidget * = 0);

    bool lineNumbersMarginEnabled() const;
    int lineNumbersMarginWidth() const;
    bool lineNumbersMarginAutoWidth() const;
    bool copyAvailable();
    bool canPaste();
    QPoint cursorPosition() const;
    void gotoLine (int line, bool moveTop);
    bool gotoBrace();
    QString getFileName() const;
    bool isNewFile();

    bool toggleBreakpoint(int &line, bool cursor = false, QString cond = QString());
    bool doToggleBreakpont(int line, QString cond = QString());
    void getAllBreakpoints(QMap<int, QString> & brmap);

    void search(QString expr, bool re, bool cs, bool whole, bool forward);
    void searchNext();
    void searchPrev();
    void replaceCur(QString srcexpr,QString desexpr,bool re, bool cs, bool whole,bool replaceAll);

    void setEditorProperties();
    void setFileOption();
    QString getCompileOption();
    QString getExecOption();

    uint lastModified () const;
    bool isModified();
    QString getCurLine();
    void showTooltip(QString function);
    //void refreshBreakpointsRecord(); //clear these unnessary breakpoints after delete from the editor.

public slots:
    void setLineNumbersMarginEnabled( bool );
    void setLineNumbersMarginWidth( int );
    void setLineNumbersMarginAutoWidth( bool );
    bool openFile( const QString&);
    bool saveFile( const QString& = QString() );
    bool saveBackup( const QString& );
    bool closeFile(bool discard = false);
    void print( bool = false );
    void quickPrint();
    void selectNone();
    void invokeGoToLine();
    void convertTabs( int = -1 );
    void makeBackup();

    void setStepMarker (int line = -1);

    virtual void setModified (bool m);
    void slotAddVarWatch();
    void slotComment();
    void clickToggleBreakpoint(int margin, int line, Qt::KeyboardModifiers);

protected:
    void keyPressEvent( QKeyEvent* );
    void wheelEvent(QWheelEvent *);
    void contextMenuEvent(QContextMenuEvent *event);
    void mouseMoveEvent(QMouseEvent* event);
    char myGetCharacter(long &pos);
    QString myGetWord(long &pos);

    bool m_copyAvailable;
    static bool m_pasteAvailableInit;
    static bool m_pasteAvailable;
    QPoint m_cursorPosition;

protected slots:
    void linesChanged();
    void setCopyAvailable( bool );
    void cursorPositionChanged( int, int );
    void textChanged();
    void clipboardDataChanged();
    void slotEditorModified(bool);

signals:
    void cursorPositionChanged( const QPoint& );
    void undoAvailable( bool );
    void redoAvailable( bool );
    void pasteAvailable( bool );
    void editorModified( MEditor *, bool);
    void bkptToggleByClicked(QString filename, int line);
    void condBkptAdded(QString condition);

private:
    bool m_modified;
    int m_breakMarker;
    int m_condBkptMarker;
    int m_stepMarker;
    int m_stepHandle;

    QString m_findExpr;
    bool m_regExpr;
    bool m_caseSensitive;
    bool m_wholeWord;
    bool m_findForward;

    uint m_lastModified;

    FileOption m_options;
    QMap<int, QString> m_bkpts;
    QString curLine;
    bool pasSrc;

    QString helpHtml;
    EditorTabWidget *m_editorTab;
};

inline uint MEditor::lastModified() const
{
    return m_lastModified;
}

inline bool MEditor::isModified()
{
    return m_modified;
}

#endif // MEDITOR_H
