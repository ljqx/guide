#ifndef EDITORTABWIDGET_H
#define EDITORTABWIDGET_H

#include <QTabWidget>

class QToolButton;
class QFileSystemWatcher;
class MEditor;
class MainWindow;

class EditorTabWidget : public QTabWidget
{
    Q_OBJECT

public:
    EditorTabWidget(QWidget *parent = 0);

    bool hasFileModified();

    int newFile ();
    int openFile (const QString &file, bool reload = false);
    void closeCurrentTab();
    void closeAllTabs();
    void closeAllTabs(bool flag);
    void closeOtherTabs();

    void setAllTabsReadOnly(bool flag);

    bool currentFileName(QString &fn);
    bool isCurrentModified();
    bool isCurrentNewFile();

    void invokeGotoLine();
    void gotoLine(int line, bool toBegin = true, bool highlight = false);
    bool gotoBrace();

    void search(QString expr, bool re, bool cs, bool whole, bool forward);
    void searchNext();
    void searchPrev();
    void replace(QString srcexpr,QString desexpr, bool re, bool cs, bool whole,bool replaceAll);

    void setCurrentCaretLineVisible(bool v);
    void setAllCaretLineVisible(bool v);

    void setEditorProperties();

    QString getCurrentCompileOption();
    QString getExecOption(QString filename);

    MEditor    *editor(const QString & filename, bool openWhenFailed = false);
    MEditor *editor(int i);
    MEditor *currentEditor();

    void toZoom(int zoom);

public slots:
    void copy();
    void cut();
    void paste();
    bool save(int index = -1);
    bool saveAs();
    bool saveAll();
    void selectAll(bool flag = true);
    void undo();
    void redo();
    void slotAddVarWatch(QString var);
    void slotSetFileOption(bool byAction = false);

protected:
    //void mousePressEvent( QMouseEvent * event );
    bool eventFilter(QObject *obj, QEvent *event);

private:
    bool swapTabs(int index1, int index2);
    bool doCloseTab (int index);

    int m_clickedItem;
    QToolButton *m_cross;
    QPoint m_mousePos;
    bool m_closeButtonInTabs;
    int m_modifiedFiles;
    int m_zoomFactor;

    QFileSystemWatcher *m_fsWatcher;
    MainWindow *m_mainWindow;

private slots:
    void slotCloseTab();
    void slotCloseOtherTab();
    void slotCloseAllTab();
    void slotEditorModify(MEditor *e, bool m);
    void slotFileChanged(const QString & path);

signals:
    void fileClosed(int index);
    void copyAvailable(bool);
    void AddVarWatch(QString var);
};
#endif    //EDITORTABWIDGET_H
