#ifndef TREECOMBOBOX_H
#define TREECOMBOBOX_H

#include <QWidget>
#include <QModelIndex>
#include <QPointer>

class QFrame;
class QTreeView;
class QAbstractItemModel;

class TreeComboBox : public QWidget
{
    Q_OBJECT

public:
    TreeComboBox( QWidget* = 0 );
    ~TreeComboBox();

    bool eventFilter( QObject*, QEvent* );

    virtual QSize sizeHint() const;
    int count() const;

    virtual QSize iconSize() const;
    virtual void setIconSize( const QSize& );

    virtual void hidePopup();
    virtual void showPopup();

    QTreeView* view() const;
    void setView( QTreeView* );

    QAbstractItemModel* model() const;
    void setModel( QAbstractItemModel* );

    QModelIndex rootIndex() const;
    void setRootIndex( const QModelIndex& );

    QModelIndex currentIndex() const;
    void setCurrentIndex( const QModelIndex& );

    void expandAll();

protected slots:
    void internal_activated( const QModelIndex& );
    void internal_clicked( const QModelIndex& );
    void internal_currentChanged( const QModelIndex&, const QModelIndex& );

protected:
    QSize m_iconSize;
    QSize m_sizeHint;
    QFrame* m_frame;
    QPointer<QTreeView> m_view;
    QPointer<QAbstractItemModel> m_model;
    QModelIndex m_index;
    bool m_force;

    void paintEvent( QPaintEvent* );
    void hideEvent( QHideEvent* );
    void enterEvent( QEvent* );
    void leaveEvent( QEvent* );
    void mousePressEvent( QMouseEvent* );

    void calculPopupGeometry();

signals:
    void activated( const QModelIndex& );
    void clicked( const QModelIndex& );
    void currentChanged( const QModelIndex& );
    void highlighted( const QModelIndex& );

};

#endif // TREECOMBOBOX_H
