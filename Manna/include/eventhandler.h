#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include <QEvent>

class MainWindow;

//
// Event Definition
//
#define ME_TARGET_QUIT (QEvent::User + 1)

class MEventHandler : public QObject
{
    Q_OBJECT
public:
    MEventHandler (MainWindow * mainWindow);

protected:
    bool eventFilter (QObject * obj, QEvent * event);
private:
    MainWindow *m_mainWindow;
};

class MEventTargetQuit : public QEvent
{
public:
    MEventTargetQuit () : QEvent (static_cast<Type> (ME_TARGET_QUIT))
    {}
};

#endif
