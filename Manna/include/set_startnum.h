#ifndef SET_STARTNUM
#define SET_STARTNUM

#include "ui_Set_startnum.h"
//#include <QOBJECT>
#include <QDialog>

class Set_startnum : public QDialog, public Ui::setting_startnum
{
    Q_OBJECT
    public:
        Set_startnum(int& cur_pos,int array_length,int &show_length);

    private slots:
        //void slotsliderMoved();
        //void slotvalueChanged();
        void slotSpinBoxValueChanged(int value);
        void slotSpinBoxLengthValueChanged(int value);
        void slotSliderValueChanged(int value);
        void slotsOK();
        void slotsCancel();
    private:
        int* set_num;
        int* arraylength;
        int* showlength;
};

#endif
