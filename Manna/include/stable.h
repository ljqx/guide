// This is precompiled header for Manna

#if defined __cplusplus

#include <QtCore>
#include <QtGui>
#include <qscintilla.h>

#include <QDebug>

class TraceObj
{
public:
    TraceObj (const char * prompt) : m_prompt (prompt)
    {
        qDebug ("-->%s", prompt);
    }
    ~TraceObj ()
    {
        qDebug ("<--%s", m_prompt);
    }

private:
    const char * m_prompt;
};

#define TRACEFUNC(func)    TraceObj(#func)

#endif 
