#ifndef FILEOPTIONDLG_H
#define FILEOPTIONDLG_H

#include "ui_fileoptiondlg.h"

class FileOptionDlg : public QDialog, public Ui::FileOptionDlg
{
    Q_OBJECT

public:
    FileOptionDlg(QWidget * parent = 0, Qt::WindowFlags f = 0);
    void setCompileCmd(QString &cmd);
    void setCompileOption(QString &opt);
    QString getCompileOption();
    void setExecOption(QString &opt);
    QString getExecOption();

};
#endif //FILEOPTIONDLG_H
