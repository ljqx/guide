#ifndef EXECUTER_H
#define EXECUTER_H

#include <QThread>
#include <QProcess>
#include "sourcetype.h"

class QTimer;

class Executer : public QThread
{
Q_OBJECT
public:
    Executer(QObject * parent, QString dirname, QString exe, QString param);
    virtual void run();
    ~Executer();

private:
    /* Variables */
    QObject *m_parent;
    QString m_executableName;
    QString m_workDirectory;
    QString m_parameters;
    QProcess *m_target;
    QString m_stdoutStr, m_stderrStr;
    QTimer *m_readTimer;

    /* Exec result */
    bool m_started;
    int m_exitCode;
    QProcess::ExitStatus m_status;

    void setEnvironment(QProcess *);

signals:
    void stopExecute();
    void outputMessage(QString, bool bStdErr);
    void execFinished(bool, int, QProcess::ExitStatus);

protected slots:
    void slotStopExec();
    void slotTargetFinished(int, QProcess::ExitStatus);
    void slotOutputMessage();
    void slotThreadFinished();
    void slotTargetStarted();
};
#endif //EXECUTER_H
