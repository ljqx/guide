#ifndef FINDWIDGET_H
#define FINDWIDGET_H

#include "ui_findwidget.h"

class QLineEdit;

class FindWidget : public QWidget, public Ui::FindWidget
{
    Q_OBJECT

public:
    FindWidget( QWidget * parent = 0, Qt::WindowFlags f = 0);

signals:
    void findRequest (QString expr, bool regex, bool cs, bool whole, bool forward);
    void replaceRequest (QString srcexpr,QString desexpr,bool regex, bool cs, bool whole,bool replaceAll);

private slots:
    void slotFindNext();
    void slotFindPrev();
    void slotReplace();
    void slotReplaceAll();
    void slotFind(QString expr);
private:
    QLineEdit *m_lineEdit;
    QLineEdit *m_replaceEdit;
};
#endif    //FINDWIDGET_H
