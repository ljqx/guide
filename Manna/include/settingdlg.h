#ifndef SETTINGDLG_H
#define SETTINGDLG_H

#include "ui_settingdlg.h"

class QsciLexer;

class SettingDialog : public QDialog, public Ui::SettingDialog
{
    Q_OBJECT

public:
    SettingDialog( QWidget* = 0 );

private:
    void loadSettings();
    void saveSettings();

    QPixmap colourizedPixmap( const QColor& ) const;
    QButtonGroup* bgExternalChanges;
    QButtonGroup* bgAutoCompletionSource;
    QButtonGroup* bgCallTipsStyle;
    QButtonGroup* bgBraceMatch;
    QButtonGroup* bgEdgeMode;
    QButtonGroup* bgFoldStyle;
    QButtonGroup* bgEolMode;
    QButtonGroup* bgWhitespaceVisibility;
    QButtonGroup* bgWrapMode;
    QButtonGroup* bgStartWrapVisualFlag;
    QButtonGroup* bgEndWrapVisualFlag;
    QHash<QString,QsciLexer*> mLexers;

public slots:
    void accept();
    void apply();

signals:
    void applySettings();

private slots:
    void on_twMenu_itemSelectionChanged();
    void tbColours_clicked();
    void tbFonts_clicked();
    void cbSourceAPIsLanguages_beforeChanged( int );
    void on_cbSourceAPIsLanguages_currentIndexChanged( int );
    void on_pbSourceAPIsDelete_clicked();
    void on_pbSourceAPIsAdd_clicked();
    void on_pbSourceAPIsBrowse_clicked();
    void on_twLexersAssociations_itemSelectionChanged();
    void on_pbLexersAssociationsAddChange_clicked();
    void on_pbLexersAssociationsDelete_clicked();
    void on_cbLexersHighlightingLanguages_currentIndexChanged( const QString& );
    void on_lwLexersHighlightingElements_itemSelectionChanged();
    void lexersHighlightingColour_clicked();
    void lexersHighlightingFont_clicked();
    void on_cbLexersHighlightingFillEol_clicked( bool );
    void cbLexersHighlightingProperties_clicked( bool );
    void on_cbLexersHighlightingIndentationWarning_currentIndexChanged( int );
    void on_pbLexersHighlightingReset_clicked();

};

#endif //SETTINGDLG_H
