#ifndef BREAKPOINTSMANAGER_H
#define BREAKPOINTSMANAGER_H

#include<QTreeWidget>
#include <QScrollArea>
#include <QLabel>
#include <QCheckBox>
#include "ui_breakpoints.h"

class MainWindow;
class EditorTabWidget;
class DataLine;

class BreakpointsManager : public QDialog, public Ui::bkptsDlg
{
    Q_OBJECT
public:
    BreakpointsManager(MainWindow *parent = 0);
    ~BreakpointsManager();

    QList<QTreeWidget*> bkpts;
    void showBreakpoints();
signals:
    void delSignal(QString filename, int line);
    void conditionChanged(QString file, int line, QString condition);
    void delAllSignal(QString filename);
private slots:
//    void select(QModelIndex index);
    void selectFile(int index);
    void selectAll();
    void selectInvert();
    void deleteSelected();
    void applyChange();
//    void select(QString fn);
//    void delSelectedBkpts();
//    void delAllBkpts();

private:
    QString currentFilename();

    EditorTabWidget *m_editorTab;
    QList<DataLine*> m_datas;

};

class DataLine : public QObject
{
    Q_OBJECT
public:
    DataLine (int number, int line, QString condition);
    ~DataLine ();

    bool selected();
    bool conditionIsDirty();
    bool deleteIsDirty();

    QCheckBox   *checkBox;
    QLabel      *number;
    QLabel      *line;
    QLineEdit   *condition;
    QPushButton *deleteState;

private slots:
    void toggleState(bool checked);
    void makeConditionDirty(QString);
private:
    bool conditionDirty;
    bool deleteDirty;
};

//class FilenameListModel : public QAbstractListModel
//{
//    Q_OBJECT

//public:
//    FilenameListModel (const QStringList &strings, QObject *parent = 0)
//        : QAbstractListModel (parent), filenameList (strings) {}

//    int rowCount(const QModelIndex &/*parent*/) const;
//    QVariant data(const QModelIndex &index, int role) const;
//    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

//private:
//    QStringList filenameList;
//};

//class BreakpointsModel : public QAbstractTableModel
//{
//    Q_OBJECT

//public:
//    BreakpointsModel (const QMap<int, QString> &map, QString fn, QObject *parent = 0)
//        : QAbstractTableModel (parent), breakpoints (map), filename (fn) {}

//    int rowCount(const QModelIndex &/*parent*/) const;
//    int columnCount(const QModelIndex &/*parent*/) const;
//    QVariant data(const QModelIndex &index, int role) const;
//    bool setData(const QModelIndex &index, const QVariant &value, int role);
//    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
//    Qt::ItemFlags flags(const QModelIndex &index) const;

//    bool removeByLine(int line);
//    void removeAll();

//signals:
//    void condChanged(QString filename, int line, QString cond);

//private:
//    QMap<int, QString> breakpoints;
//    QString filename;
//};

//class BreakpointsWidget : public QScrollArea
//{
//    Q_OBJECT
//public:
//    BreakpointsWidget (QWidget *parent = 0)
//        : QScrollArea (parent) {}

//    void setBreakpoints(QMap<int, QString> breakpoints);

//private slots:
//    //void deleteSelected();

//private:
//    QMap<int, QString> breakpoints;
//};

//class BreakpointsView : public QAbstractItemView
//{
//    Q_OBJECT

//public:
//    BreakpointsView (QWidget *parent = 0);

//protected slots:
//    void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
//    void rowsInserted(const QModelIndex &parent, int start, int end);
//    void rowsAboutToBeRemoved(const QModelIndex &parent, int start, int end);

//private:

//};

#endif // BREAKPOINTSMANAGER_H
