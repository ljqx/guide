#ifndef BUILD_H
#define BUILD_H

#include <QThread>
#include <QProcess>
#include "sourcetype.h"

class Build : public QThread
{
Q_OBJECT
public:
    Build(QObject * parent, QString dirname, QString filename, QString cmdline, SourceType type);
    void run();
    int nbErrors() { return m_errors; }
    int nbWarnings() { return m_warnings; }
    QString srcFileName() { return m_fullName; }

private:
    QString m_targetDirecotry;
    QString m_fullName;
    QString m_cmdline;
    SourceType m_type;

    bool m_isStopped;
    QProcess *m_buildProcess;

    int m_errors;
    int m_warnings;

    QString m_stderrBuf;
    QString m_stdoutBuf;

signals:
    void message(QString, QString="", SourceType=TYPE_C_CPP);
    void buildFinished(bool success);

protected slots:
    void slotBuildMessages();
    void slotBuildFinished(int exitcode, QProcess::ExitStatus exitStatus);

public slots:
    void slotIncErrors();
    void slotIncWarnings();
    void slotStopBuild();
};

#endif
