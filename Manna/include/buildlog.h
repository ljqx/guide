#ifndef BUILDLOG_H
#define BUILDLOG_H

#include <QTextEdit>
#include <QTextBlockUserData>
#include "build.h"
#include "sourcetype.h"

class BlockLogBuild : public QTextBlockUserData
{
public:
    BlockLogBuild(QString d, SourceType type)
        : QTextBlockUserData() {
        m_directory = d;
        m_type = type;
    }
    QString directory() { return m_directory; }
    SourceType sourceType() {return m_type;}
private:
    QString m_directory;
    SourceType m_type;
};


class BuildLog : public QTextEdit
{
Q_OBJECT
public:
    BuildLog(QWidget * parent = 0);

    static bool containsError(QString dirname, QString message, SourceType type);
    static bool containsWarning(QString dirname, QString message, SourceType type);
protected:
    void mouseDoubleClickEvent( QMouseEvent * event );

public slots:
//    void slotMessagesStdout(QString list, QString directory);
    void slotMessagesBuild(QString list, QString directory, SourceType type = TYPE_C_CPP);

private:
    enum MessageType {MT_NORMAL, MT_ERROR, MT_WARNING};

    MessageType parseGccOutput(QString dirname, QString message, bool sig = false);
    MessageType parseFpcOutput(QString dirname, QString message, bool sig = false);

signals:
    void incErrors();
    void incWarnings();
    void buildLogClicked(QString, int);
};
#endif    //BUILDLOG_H
