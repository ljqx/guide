#ifndef VARTREEMODEL_H
#define VARTREEMODEL_H

#include <QVariant>
#include <list>

class GDBMIVariableClient;
class QTreeWidgetItem;

#define MIV_NORMAL    0
#define MIV_EXPAND    0x1
#define MIV_VAROBJ    0x2
#define MIV_ROOT    0x4
#define MIV_MARK    0x8
#define MIV_MORE    0x10

class MIVariable
{
public:
    typedef unsigned int STATUS;

    MIVariable (const QString & name, STATUS status = MIV_NORMAL);
    virtual ~MIVariable ();

    int    indexOf (MIVariable * var);
    MIVariable * child (int index);
    MIVariable    * firstChild ();
    MIVariable    * lastChild ();

    void addChild (MIVariable * child);
    void removeAllChild ();

    STATUS status () const        { return m_status;    }
    void setStatus (STATUS s);
    void clearStatus (STATUS s);

    MIVariable * findVariable (const QString & name);
    MIVariable * findVariable (const QString & name,bool isPascal);

public:
    typedef std::list<MIVariable *>    VARLIST;

    QString        m_name;
    QString        m_type;
    QString        m_value;
    STATUS        m_status;
    bool        m_isComplex;
    int            m_childnum;

    MIVariable    * m_parent;
    VARLIST        m_children;
};

Q_DECLARE_METATYPE (MIVariable *)

class VarTreeManager : public QObject
{
    Q_OBJECT

public:
    VarTreeManager (GDBMIVariableClient * client) : m_client (client)
    {}

public slots:
    void onItemCollapsed (QTreeWidgetItem * )
    {

    }

    void onItemExpanded (QTreeWidgetItem * );
    void onItemDoubleClicked(QTreeWidgetItem * ,int);

    void onItemSelectionChanged ()
    {

    }

protected:
    GDBMIVariableClient        * m_client;
};

inline MIVariable::MIVariable (const QString & name, STATUS status)
    : m_name (name), m_status (status), m_parent (0), m_children (0)
{
    m_isComplex = false;
}

inline int MIVariable::indexOf (MIVariable * var)
{
    int index = 0;
    for (VARLIST::iterator it = m_children.begin ();
        it != m_children.end ();
        ++ it, ++ index)
    {
        if ((* it) == var)
            return index;
    }

    return -1;
}

inline MIVariable * MIVariable::child (int index)
{
    int i = 0;
    for (VARLIST::iterator it = m_children.begin ();
        it != m_children.end ();
        ++ it, ++ i)
    {
        if (i == index)
            return (* it);
    }

    return 0;
}

inline MIVariable * MIVariable::firstChild ()
{
    return m_children.empty () ? 0 : m_children.front ();
}

inline MIVariable * MIVariable::lastChild ()
{
    return m_children.empty () ? 0 : m_children.back ();
}

inline void MIVariable::setStatus (STATUS s)
{
    m_status |= s;
}

inline void MIVariable::clearStatus (STATUS s)
{
    m_status &= (~s);
}

inline void MIVariable::addChild (MIVariable * child)
{
    child->m_parent = this;
    m_children.push_back (child);
}

#endif
