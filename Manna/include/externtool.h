#ifndef EXTERNTOOL_H
#define EXTERNTOOL_H
//
#include "ui_externtool.h"
//
class ExternTool : public QDialog, public Ui::ExternTool
{
    Q_OBJECT

public:
    ExternTool( QWidget * parent = 0, Qt::WindowFlags f = 0 );
    QString gccPath() { return gcc->text(); }
    QString fpcPath() { return fpc->text(); }
    void writeSettings();

    static void checkExternTool();
    static void resetExternTool();
    static void setEnvironment();

    bool testGcc(bool silent = false, QString dir = QString());
    bool testGpp(bool silent = false, QString dir = QString());
    bool testGdb(bool silent = false, QString dir = QString());
    bool testFpc(bool silent = false, QString dir = QString());
    bool checkExistence(QString name, bool silent = false, QString dir = QString());

private slots:
    void on_gccLocation_clicked();
    void on_fpcLocation_clicked();
    void on_test_clicked();
    void on_buttonBox_clicked(QAbstractButton * button );
    void chooseLocation(QLineEdit *dest);

private:
    bool testExternTool(bool silent = false);
    void setDefaultPath();
    void setPathToEnvironment();
};
#endif










