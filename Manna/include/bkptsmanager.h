#ifndef BKPTSMANAGER_H
#define BKPTSMANAGER_H

#include<QTreeWidget>
#include "ui_bkpts.h"

class MainWindow;
class EditorTabWidget;
class BreakpointsModel;

class BkptsManager : public QDialog, public Ui::BkptsDialog
{
        Q_OBJECT
public:
    BkptsManager(MainWindow *parent = 0);

private slots:
    void selectFile(int i);

private:
    MainWindow *m_mainWindow;
    EditorTabWidget *m_editorTab;
    BreakpointsModel *m_bkptsModel;
};

class BreakpointsModel : public QAbstractTableModel
{
        Q_OBJECT

public:
    BreakpointsModel (QObject *parent = 0)
        : QAbstractTableModel (parent) {}
    BreakpointsModel (const QMap<int, QString> map, QString f, QObject *parent = 0)
        : QAbstractTableModel (parent), breakpoints (map), file (f) {
        for (QMap<int, QString>::ConstIterator i = breakpoints.begin();
             i != breakpoints.end(); i++) {
            checkState.insert(i.key(), Qt::Unchecked);
        }
    }

    void setBreakpoints(const QMap<int, QString> map, QString f);

    int rowCount(const QModelIndex &/*parent*/) const;
    int columnCount(const QModelIndex &/*parent*/) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

signals:
    void deleteBkpt(QString file, int line);
    void condChanged(QString file, int line, QString cond);

private slots:
    void selectAll();
    void selectInvert();
    void deleteSelected();

private:
    QMap<int, QString> breakpoints;
    QMap<int, Qt::CheckState> checkState;
    QString file;
    enum {
        CheckColumn = 0,
        LineColumn = 1,
        ConditionColumn = 2,
        ColumnCount = 3
    };
};

#endif // BKPTSMANAGER_H
