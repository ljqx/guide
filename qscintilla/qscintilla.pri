# Used by Manna and qscintilla

# define where is the official qscintilla folder
QSCINTILLAVERSION	= QScintilla-gpl-2.8

# include path
INCLUDEPATH	*= $${PWD}/sdk
INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/Qt4Qt5
