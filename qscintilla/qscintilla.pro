# For including qscintilla

include( qscintilla.pri )

# include original project
include( $$QSCINTILLAVERSION/Qt4Qt5/qscintilla.pro )

INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/Qt4Qt5
INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/Qt4Qt5/Qsci
INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/include
INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/src
INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/lexers
INCLUDEPATH	*= $${PWD}/$$QSCINTILLAVERSION/lexlib

# depend path
greaterThan(QT_MAJOR_VERSION, 4) {
    VPATH *= $${PWD}/$$QSCINTILLAVERSION/Qt4Qt5
} else {
    DEPENDPATH *= $${PWD}/$$QSCINTILLAVERSION/Qt4Qt5
}

BUILD_PATH	= ../build
DESTDIR	= $${BUILD_PATH}

# overwrite some values to made no lib
CONFIG	-= dll
CONFIG	+= staticlib

CONFIG(DebugBuild)|CONFIG(debug, debug|release) {
	#Debug
	CONFIG	+= console
	unix:TARGET	= $$join(TARGET,,,_debug)
	else:TARGET	= $$join(TARGET,,,d)
	unix:OBJECTS_DIR	= $${BUILD_PATH}/debug/.obj/unix
	win32:OBJECTS_DIR	= $${BUILD_PATH}/debug/.obj/win32
	mac:OBJECTS_DIR	= $${BUILD_PATH}/debug/.obj/mac
	UI_DIR	= $${BUILD_PATH}/debug/.ui
	MOC_DIR	= $${BUILD_PATH}/debug/.moc
	RCC_DIR	= $${BUILD_PATH}/debug/.rcc

} else {
	#Release
	unix:OBJECTS_DIR	= $${BUILD_PATH}/release/.obj/unix
	win32:OBJECTS_DIR	= $${BUILD_PATH}/release/.obj/win32
	mac:OBJECTS_DIR	= $${BUILD_PATH}/release/.obj/mac
	UI_DIR	= $${BUILD_PATH}/release/.ui
	MOC_DIR	= $${BUILD_PATH}/release/.moc
	RCC_DIR	= $${BUILD_PATH}/release/.rcc
}
