/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     MI_NL = 258,
     MI_GDB = 259,
     MI_CSTRING = 260,
     MI_STRING = 261,
     MI_TOKEN = 262,
     MIC_DONE = 263,
     MIC_RUNNING = 264,
     MIC_CONNECTED = 265,
     MIC_ERROR = 266,
     MIC_EXIT = 267,
     MIC_STOPPED = 268
   };
#endif
/* Tokens.  */
#define MI_NL 258
#define MI_GDB 259
#define MI_CSTRING 260
#define MI_STRING 261
#define MI_TOKEN 262
#define MIC_DONE 263
#define MIC_RUNNING 264
#define MIC_CONNECTED 265
#define MIC_ERROR 266
#define MIC_EXIT 267
#define MIC_STOPPED 268




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 24 "miparser.y"
typedef union YYSTYPE {
	MIGDBOutput			* pGDBOutput;
	MIOutOfBandRecord	* pOutOfBandRecord;
	MIAsyncRecord		* pAsyncRecord;
	MIStreamRecord		* pStreamRecord;
	MIResultRecord		* pResultRecord;
	MIResult			* pResult;
	MIValue				* pValue;
	MIList				* pList;
	MIString			strval;
	int					intval;
	MIAsyncClass		async_class;
	MIResultClass		result_class;
} YYSTYPE;
/* Line 1447 of yacc.c.  */
#line 79 "miparser.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



