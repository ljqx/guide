/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     MI_NL = 258,
     MI_GDB = 259,
     MI_CSTRING = 260,
     MI_STRING = 261,
     MI_TOKEN = 262,
     MIC_DONE = 263,
     MIC_RUNNING = 264,
     MIC_CONNECTED = 265,
     MIC_ERROR = 266,
     MIC_EXIT = 267,
     MIC_STOPPED = 268
   };
#endif
/* Tokens.  */
#define MI_NL 258
#define MI_GDB 259
#define MI_CSTRING 260
#define MI_STRING 261
#define MI_TOKEN 262
#define MIC_DONE 263
#define MIC_RUNNING 264
#define MIC_CONNECTED 265
#define MIC_ERROR 266
#define MIC_EXIT 267
#define MIC_STOPPED 268




/* Copy the first part of user declarations.  */
#line 3 "miparser.y"

#include "midef.h"

#ifdef UNITTEST
#include <stdio.h>

void	PrintGDBOutput (MIGDBOutput * output);
#endif

MIGDBOutput * ParseMIOutput (const char * output);

int		yylex ();
void	yyerror (char const *);
void	InitLexer (const char * input);

//MIResult * ReverseResults(MIResult* head); //add by porky 200903

static MIGDBOutput * s_pResult;



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 24 "miparser.y"
typedef union YYSTYPE {
	MIGDBOutput			* pGDBOutput;
	MIOutOfBandRecord	* pOutOfBandRecord;
	MIAsyncRecord		* pAsyncRecord;
	MIStreamRecord		* pStreamRecord;
	MIResultRecord		* pResultRecord;
	MIResult			* pResult;
	MIValue				* pValue;
	MIList				* pList;
	MIString			strval;
	int					intval;
	MIAsyncClass		async_class;
	MIResultClass		result_class;
} YYSTYPE;
/* Line 196 of yacc.c.  */
#line 147 "miparser.cpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 159 "miparser.cpp"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  40
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   66

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  26
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  25
/* YYNRULES -- Number of rules. */
#define YYNRULES  48
/* YYNRULES -- Number of states. */
#define YYNSTATES  84

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   268

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    25,     2,
       2,     2,    16,    17,    15,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    18,     2,     2,    24,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    21,     2,    22,    14,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    19,     2,    20,    23,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned char yyprhs[] =
{
       0,     0,     3,     5,     8,    12,    15,    18,    20,    25,
      29,    30,    32,    34,    36,    38,    40,    44,    47,    51,
      54,    58,    61,    65,    67,    69,    71,    73,    75,    77,
      79,    83,    85,    87,    89,    93,    94,    96,    99,   104,
     107,   112,   117,   119,   121,   123,   127,   131,   135
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      27,     0,    -1,    28,    -1,     4,     3,    -1,    29,     4,
       3,    -1,    32,    28,    -1,    50,    30,    -1,    30,    -1,
      14,    38,    31,     3,    -1,    15,    40,    31,    -1,    -1,
      33,    -1,    46,    -1,    34,    -1,    35,    -1,    36,    -1,
      50,    16,    37,    -1,    16,    37,    -1,    50,    17,    37,
      -1,    17,    37,    -1,    50,    18,    37,    -1,    18,    37,
      -1,    39,    31,     3,    -1,     8,    -1,     9,    -1,    10,
      -1,    11,    -1,    12,    -1,    13,    -1,     6,    -1,     6,
      18,    41,    -1,    43,    -1,    44,    -1,    45,    -1,    15,
      41,    42,    -1,    -1,     5,    -1,    19,    20,    -1,    19,
      40,    31,    20,    -1,    21,    22,    -1,    21,    41,    42,
      22,    -1,    21,    40,    31,    22,    -1,    47,    -1,    48,
      -1,    49,    -1,    23,     5,     3,    -1,    24,     5,     3,
      -1,    25,     5,     3,    -1,     7,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned char yyrline[] =
{
       0,    61,    61,    63,    66,    69,    73,    74,    76,    83,
      84,    86,    89,    93,    94,    95,    97,    98,   100,   101,
     103,   104,   106,   110,   111,   112,   113,   114,   116,   117,
     120,   123,   124,   125,   127,   128,   130,   132,   133,   135,
     136,   138,   141,   142,   143,   145,   149,   153,   157
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "MI_NL", "MI_GDB", "MI_CSTRING",
  "MI_STRING", "MI_TOKEN", "MIC_DONE", "MIC_RUNNING", "MIC_CONNECTED",
  "MIC_ERROR", "MIC_EXIT", "MIC_STOPPED", "'^'", "','", "'*'", "'+'",
  "'='", "'{'", "'}'", "'['", "']'", "'~'", "'@'", "'&'", "$accept",
  "migrammer", "output", "result_record", "result_output", "results",
  "out_of_band_record", "async_record", "exec_async_output",
  "status_async_output", "notify_async_output", "async_output",
  "result_class", "async_class", "result", "value", "values", "const",
  "tuple", "list", "stream_record", "console_stream_output",
  "target_stream_output", "log_stream_output", "token", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,    94,    44,    42,    43,    61,   123,
     125,    91,    93,   126,    64,    38
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    26,    27,    28,    28,    28,    29,    29,    30,    31,
      31,    32,    32,    33,    33,    33,    34,    34,    35,    35,
      36,    36,    37,    38,    38,    38,    38,    38,    39,    39,
      40,    41,    41,    41,    42,    42,    43,    44,    44,    45,
      45,    45,    46,    46,    46,    47,    48,    49,    50
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     2,     3,     2,     2,     1,     4,     3,
       0,     1,     1,     1,     1,     1,     3,     2,     3,     2,
       3,     2,     3,     1,     1,     1,     1,     1,     1,     1,
       3,     1,     1,     1,     3,     0,     1,     2,     4,     2,
       4,     4,     1,     1,     1,     3,     3,     3,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,     0,    48,     0,     0,     0,     0,     0,     0,     0,
       0,     2,     0,     7,     0,    11,    13,    14,    15,    12,
      42,    43,    44,     0,     3,    23,    24,    25,    26,    27,
      10,    29,    28,    17,    10,    19,    21,     0,     0,     0,
       1,     0,     5,     0,     0,     0,     6,     0,     0,     0,
      45,    46,    47,     4,    16,    18,    20,     0,    10,     8,
      22,     0,     9,    36,     0,     0,    30,    31,    32,    33,
      37,    10,    39,    10,    35,     0,     0,     0,     0,    38,
      41,    35,    40,    34
};

/* YYDEFGOTO[NTERM-NUM]. */
static const yysigned_char yydefgoto[] =
{
      -1,    10,    11,    12,    13,    48,    14,    15,    16,    17,
      18,    33,    30,    34,    58,    66,    78,    67,    68,    69,
      19,    20,    21,    22,    23
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -59
static const yysigned_char yypact[] =
{
      -2,    10,   -59,    36,    19,    19,    19,    13,    24,    25,
       3,   -59,    30,   -59,    -2,   -59,   -59,   -59,   -59,   -59,
     -59,   -59,   -59,    35,   -59,   -59,   -59,   -59,   -59,   -59,
      20,   -59,   -59,   -59,    20,   -59,   -59,    33,    34,    39,
     -59,    47,   -59,    19,    19,    19,   -59,    48,    52,    53,
     -59,   -59,   -59,   -59,   -59,   -59,   -59,    40,    20,   -59,
     -59,    12,   -59,   -59,     0,     5,   -59,   -59,   -59,   -59,
     -59,    20,   -59,    20,    42,    41,    37,    12,    38,   -59,
     -59,    42,   -59,   -59
};

/* YYPGOTO[NTERM-NUM].  */
static const yysigned_char yypgoto[] =
{
     -59,   -59,    49,   -59,    43,   -30,   -59,   -59,   -59,   -59,
     -59,    -5,   -59,   -59,   -56,   -58,   -19,   -59,   -59,   -59,
     -59,   -59,   -59,   -59,   -59
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const unsigned char yytable[] =
{
      35,    36,     1,    40,    49,     2,    57,    74,    71,    73,
      63,    57,     3,    24,     4,     5,     6,    63,    37,    81,
      70,     7,     8,     9,    64,    31,    65,    72,    62,    38,
      39,    64,    32,    65,    41,    47,    50,    51,    54,    55,
      56,    75,    52,    76,    25,    26,    27,    28,    29,     3,
      53,    43,    44,    45,    57,    59,    60,    77,    61,    80,
      82,    79,    83,    42,     0,     0,    46
};

static const yysigned_char yycheck[] =
{
       5,     6,     4,     0,    34,     7,     6,    65,    64,    65,
       5,     6,    14,     3,    16,    17,    18,     5,     5,    77,
      20,    23,    24,    25,    19,     6,    21,    22,    58,     5,
       5,    19,    13,    21,     4,    15,     3,     3,    43,    44,
      45,    71,     3,    73,     8,     9,    10,    11,    12,    14,
       3,    16,    17,    18,     6,     3,     3,    15,    18,    22,
      22,    20,    81,    14,    -1,    -1,    23
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,     4,     7,    14,    16,    17,    18,    23,    24,    25,
      27,    28,    29,    30,    32,    33,    34,    35,    36,    46,
      47,    48,    49,    50,     3,     8,     9,    10,    11,    12,
      38,     6,    13,    37,    39,    37,    37,     5,     5,     5,
       0,     4,    28,    16,    17,    18,    30,    15,    31,    31,
       3,     3,     3,     3,    37,    37,    37,     6,    40,     3,
       3,    18,    31,     5,    19,    21,    41,    43,    44,    45,
      20,    40,    22,    40,    41,    31,    31,    15,    42,    20,
      22,    41,    22,    42
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 100000000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);


# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()
    ;
#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 61 "miparser.y"
    {	s_pResult = (yyvsp[0].pGDBOutput);	;}
    break;

  case 3:
#line 63 "miparser.y"
    {	(yyval.pGDBOutput) = new MIGDBOutput; 
																(yyval.pGDBOutput)->m_pOutOfBandRecord = 0;
																(yyval.pGDBOutput)->m_pResultRecord = 0;	;}
    break;

  case 4:
#line 66 "miparser.y"
    {	(yyval.pGDBOutput) = new MIGDBOutput;
																(yyval.pGDBOutput)->m_pOutOfBandRecord = 0;
																(yyval.pGDBOutput)->m_pResultRecord = (yyvsp[-2].pResultRecord);	;}
    break;

  case 5:
#line 69 "miparser.y"
    {	(yyvsp[-1].pOutOfBandRecord)->m_pNext = (yyvsp[0].pGDBOutput)->m_pOutOfBandRecord;
																(yyvsp[0].pGDBOutput)->m_pOutOfBandRecord = (yyvsp[-1].pOutOfBandRecord);
																(yyval.pGDBOutput) = (yyvsp[0].pGDBOutput);	;}
    break;

  case 6:
#line 73 "miparser.y"
    {	(yyvsp[0].pResultRecord)->m_token = (yyvsp[-1].intval); (yyval.pResultRecord) = (yyvsp[0].pResultRecord);	;}
    break;

  case 7:
#line 74 "miparser.y"
    {	(yyvsp[0].pResultRecord)->m_token = 0; (yyval.pResultRecord) = (yyvsp[0].pResultRecord);	;}
    break;

  case 8:
#line 76 "miparser.y"
    {	(yyval.pResultRecord) = new MIResultRecord;
																(yyval.pResultRecord)->m_class = (yyvsp[-2].result_class);
																(yyval.pResultRecord)->m_pResult = (yyvsp[-1].pResult);;}
    break;

  case 9:
#line 83 "miparser.y"
    {	(yyvsp[-1].pResult)->m_pNext = (yyvsp[0].pResult); (yyval.pResult) = (yyvsp[-1].pResult);	;}
    break;

  case 10:
#line 84 "miparser.y"
    {	(yyval.pResult) = 0;	;}
    break;

  case 11:
#line 86 "miparser.y"
    {	(yyval.pOutOfBandRecord) = new MIOutOfBandRecord; (yyval.pOutOfBandRecord)->m_pNext = 0;
																(yyval.pOutOfBandRecord)->m_pAsyncRecord = (yyvsp[0].pAsyncRecord);
																(yyval.pOutOfBandRecord)->m_type = ASYNC;	;}
    break;

  case 12:
#line 89 "miparser.y"
    {	(yyval.pOutOfBandRecord) = new MIOutOfBandRecord; (yyval.pOutOfBandRecord)->m_pNext = 0;
																(yyval.pOutOfBandRecord)->m_pStreamRecord = (yyvsp[0].pStreamRecord);
																(yyval.pOutOfBandRecord)->m_type = STREAM;	;}
    break;

  case 13:
#line 93 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_type = EXEC; (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 14:
#line 94 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_type = STATUS; (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 15:
#line 95 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_type = NOTIFY; (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 16:
#line 97 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_token = (yyvsp[-2].intval); (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 17:
#line 98 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_token = 0; (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 18:
#line 100 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_token = (yyvsp[-2].intval); (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 19:
#line 101 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_token = 0; (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 20:
#line 103 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_token = (yyvsp[-2].intval); (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 21:
#line 104 "miparser.y"
    {	(yyvsp[0].pAsyncRecord)->m_token = 0; (yyval.pAsyncRecord) = (yyvsp[0].pAsyncRecord);	;}
    break;

  case 22:
#line 106 "miparser.y"
    {	(yyval.pAsyncRecord) = new MIAsyncRecord;
																(yyval.pAsyncRecord)->m_pResult = (yyvsp[-1].pResult);//ReverseResults($2);
																(yyval.pAsyncRecord)->m_class = (yyvsp[-2].async_class);	;}
    break;

  case 23:
#line 110 "miparser.y"
    {	(yyval.result_class) = RC_DONE;	;}
    break;

  case 24:
#line 111 "miparser.y"
    {	(yyval.result_class) = RC_RUNNING;	;}
    break;

  case 25:
#line 112 "miparser.y"
    {	(yyval.result_class) = RC_CONNECTED;	;}
    break;

  case 26:
#line 113 "miparser.y"
    {	(yyval.result_class) = RC_ERROR;	;}
    break;

  case 27:
#line 114 "miparser.y"
    {	(yyval.result_class) = RC_EXIT;	;}
    break;

  case 28:
#line 116 "miparser.y"
    {	(yyval.async_class) = STOPPED;	;}
    break;

  case 29:
#line 118 "miparser.y"
    {	(yyval.async_class) = OTHER;	;}
    break;

  case 30:
#line 120 "miparser.y"
    {	(yyval.pResult) = new MIResult; (yyval.pResult)->m_pNext = 0; (yyval.pResult)->m_variable = (yyvsp[-2].strval);
																(yyval.pResult)->m_pValue = (yyvsp[0].pValue);	;}
    break;

  case 31:
#line 123 "miparser.y"
    {	(yyval.pValue) = new MIValue; (yyval.pValue)->m_pNext = 0; (yyval.pValue)->m_type = VT_CONST; (yyval.pValue)->m_string = (yyvsp[0].strval);	;}
    break;

  case 32:
#line 124 "miparser.y"
    {	(yyval.pValue) = new MIValue; (yyval.pValue)->m_pNext = 0; (yyval.pValue)->m_type = VT_TUPLE; (yyval.pValue)->m_pResult = (yyvsp[0].pResult);	;}
    break;

  case 33:
#line 125 "miparser.y"
    {	(yyval.pValue) = new MIValue; (yyval.pValue)->m_pNext = 0; (yyval.pValue)->m_type = VT_LIST; (yyval.pValue)->m_pList = (yyvsp[0].pList);	;}
    break;

  case 34:
#line 127 "miparser.y"
    {	(yyvsp[-1].pValue)->m_pNext = (yyvsp[0].pValue); (yyval.pValue) = (yyvsp[-1].pValue);	;}
    break;

  case 35:
#line 128 "miparser.y"
    {	(yyval.pValue) = 0;	;}
    break;

  case 36:
#line 130 "miparser.y"
    {	(yyval.strval) = (yyvsp[0].strval);	;}
    break;

  case 37:
#line 132 "miparser.y"
    {	(yyval.pResult) = 0;	;}
    break;

  case 38:
#line 133 "miparser.y"
    {	(yyvsp[-2].pResult)->m_pNext = (yyvsp[-1].pResult);(yyval.pResult) = (yyvsp[-2].pResult); ;}
    break;

  case 39:
#line 135 "miparser.y"
    {	(yyval.pList) = new MIList; (yyval.pList)->m_type = EMPTY;	;}
    break;

  case 40:
#line 136 "miparser.y"
    {	(yyval.pList) = new MIList; (yyval.pList)->m_type = VALUE;
																(yyvsp[-2].pValue)->m_pNext = (yyvsp[-1].pValue); (yyval.pList)->m_pValue = (yyvsp[-2].pValue);	;}
    break;

  case 41:
#line 138 "miparser.y"
    {	(yyval.pList) = new MIList; (yyval.pList)->m_type = RESULT;
																(yyvsp[-2].pResult)->m_pNext = (yyvsp[-1].pResult); (yyval.pList)->m_pResult = (yyvsp[-2].pResult);;}
    break;

  case 42:
#line 141 "miparser.y"
    {	(yyval.pStreamRecord) = (yyvsp[0].pStreamRecord);	;}
    break;

  case 43:
#line 142 "miparser.y"
    {	(yyval.pStreamRecord) = (yyvsp[0].pStreamRecord);	;}
    break;

  case 44:
#line 143 "miparser.y"
    {	(yyval.pStreamRecord) = (yyvsp[0].pStreamRecord);	;}
    break;

  case 45:
#line 145 "miparser.y"
    {	(yyval.pStreamRecord) = new MIStreamRecord; 
																(yyval.pStreamRecord)->m_type = CONSOLE;
																(yyval.pStreamRecord)->m_string = (yyvsp[-1].strval);	;}
    break;

  case 46:
#line 149 "miparser.y"
    {	(yyval.pStreamRecord) = new MIStreamRecord; 
																(yyval.pStreamRecord)->m_type = TARGET;
																(yyval.pStreamRecord)->m_string = (yyvsp[-1].strval);	;}
    break;

  case 47:
#line 153 "miparser.y"
    {	(yyval.pStreamRecord) = new MIStreamRecord;
																(yyval.pStreamRecord)->m_type = LOG;
																(yyval.pStreamRecord)->m_string = (yyvsp[-1].strval);	;}
    break;

  case 48:
#line 157 "miparser.y"
    {	(yyval.intval) = (yyvsp[0].intval);	;}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 1470 "miparser.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (YY_("syntax error"));
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping", yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 160 "miparser.y"


void yyerror (char const * s)
{
#ifdef UNITTEST
	fprintf (stderr, "%s", s);
#endif
}

MIGDBOutput * ParseMIOutput (const char * output)
{
	InitLexer (output);
	s_pResult = 0;
	
	return yyparse () == 0 ? s_pResult : 0;
}

/* 
MIResult * ReverseResults(MIResult* head)
{
	if (head == 0) return 0;
	MIResult * point1,*point2;
	//first node
	point1 = head->m_pNext;
	if (point1 == 0) return head;
	point2 = point1->m_pNext;	
	point1 ->m_pNext = head;
	head->m_pNext = 0;
	head = point1;
	
	
    while (point2) {
		point1 = point2;
		point2 = point2->m_pNext;
		point1 -> m_pNext = head;
		head = point1;
	}
	return head;
}
*/

#ifdef UNITTEST
void PrintList (MIList * list, int tab);
void PrintValue (MIValue * value, int tab);
void PrintResult (MIResult * result, int tab);
void PrintGDBOutput (MIGDBOutput * output);

int main (int argc, char ** argv)
{	
	int i;
	int fail = 0;
	char * buffer = (char *)malloc (16 * 1024);
	
	for (i = 1; i < argc; ++i)
	{
		printf ("process %s...", argv[i]);
		
		FILE * fp = fopen (argv[i], "r");
		int c = 0;
		
		if (! fp)
		{
			printf ("failed to open %s\n", argv[i]);
			continue;
		}
		
		while ((buffer[c] = fgetc (fp)) != EOF)
			++c;
		buffer[c] = 0;
		printf ("%d characters read\n", c);
		
		MIGDBOutput * output = ParseMIOutput (buffer);
		if (output)
		{
			PrintGDBOutput (output);
			ReleaseMIOutput (output);
		}
		else
			++ fail;
			
		fclose (fp);
	}

	free (buffer);
	
	printf ("**************************************************\n");
	printf ("Result: %d fail\n", fail);
	
	return 0;
}

char * GetTabs (int tab)
{
	char * tabs = (char *)malloc (tab + 1);
	for (int i = 0; i < tab; i++)
		tabs[i] = ' ';
	tabs[tab] = '\0';

	return tabs;
}

void PrintList (MIList * list, int tab)
{
	char * tabs = GetTabs (tab);

	printf ("%s<List>\n", tabs);

	if (list->m_type == VALUE)
		PrintValue (list->m_pValue, tab + 1);
	else if (list->m_type == RESULT)
		PrintResult (list->m_pResult, tab + 1);

	printf ("%s</list>\n", tabs);

	free (tabs);
}

void PrintValue (MIValue * value, int tab)
{
	char * tabs = GetTabs (tab);

	while (value)
	{
		printf ("%s<Value>\n", tabs);

		if (value->m_type == CONST)
			printf ("%s <const>%s</const>\n", tabs, value->m_string);
		else if (value->m_type == TUPLE)
			PrintResult (value->m_pResult, tab + 1);
		else // value->m_type == LIST
			PrintList (value->m_pList, tab + 1);

		printf ("%s</Value>\n", tabs);

		value = value->m_pNext;
	}

	free (tabs);
}

void PrintResult (MIResult * result, int tab)
{
	char * tabs = GetTabs (tab);

	while (result)
	{
		printf ("%s<Result>\n%s <variable>%s</variable>\n",
			tabs, tabs, result->m_variable);
		PrintValue (result->m_pValue, tab + 1);
		printf ("%s</Result>\n", tabs);

		result = result->m_pNext;
	}

	free (tabs);
}

void PrintGDBOutput (MIGDBOutput * output)
{
	printf ("<MIGDBOutput>\n");

	MIOutOfBandRecord * pOutOfBandRecord = output->m_pOutOfBandRecord;
	while (pOutOfBandRecord)
	{
		printf (" <OutOfBandRecord>\n");
		if (pOutOfBandRecord->m_type == ASYNC)
		{
			printf ("  <type>%d</type>\n  <token>%d</token>\n  <class>%d</class>\n",
				pOutOfBandRecord->m_pAsyncRecord->m_type,
				pOutOfBandRecord->m_pAsyncRecord->m_token,
				pOutOfBandRecord->m_pAsyncRecord->m_class);
			PrintResult (pOutOfBandRecord->m_pAsyncRecord->m_pResult, 3);
		}
		else // STREAM
		{
			printf ("  <type>%d</type>\n  <string>%s</string>\n",
				pOutOfBandRecord->m_pStreamRecord->m_type,
				pOutOfBandRecord->m_pStreamRecord->m_string);
		}
		printf (" </OutOfBandRecord>\n");

		pOutOfBandRecord = pOutOfBandRecord->m_pNext;
	}

	if (output->m_pResultRecord)
	{
		printf (" <ResultRecord>\n  <class>%d</class>\n  <token>%d</token>\n",
			output->m_pResultRecord->m_class, output->m_pResultRecord->m_token);
		PrintResult (output->m_pResultRecord->m_pResult, 1);
		printf ("</ResultRecord>\n");
	}
	
	printf ("</MIGDBOutput>\n");
}
#endif
